module.exports = {
  root: false,
  env: {
    node: true,
  },
  extends: [
    "plugin:vue/strongly-recommended",
    "plugin:prettier/recommended",
    "eslint:recommended",
  ],
  rules: {
    "no-console": "off",
    semi: ["error", "always"],
    indent: ["error", 2],
    "prettier/prettier": [
      "error",
      {
        "endOfLine": "auto"
      },
    ],
  },
  parserOptions: {
    parser: "babel-eslint",
  },
  plugins: ["prettier"],
};
