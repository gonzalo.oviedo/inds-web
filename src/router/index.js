import Vue from "vue";
import Router from "vue-router";

//import store from "@/store";

const originalPush = Router.prototype.push;
Router.prototype.push = function push(location, onResolve, onReject) {
  if (onResolve || onReject)
    return originalPush.call(this, location, onResolve, onReject);
  return originalPush.call(this, location).catch((err) => err);
};

Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    {
      name: "root",
      path: "/",
      beforeEnter: (to, from, next) => {
        /*store
          .dispatch("authentication/isAuthenticated")
          .then(() => next("/login"))
          .catch(() => next("/home"));*/
        () => next("/pmo-digital");
      },
    },
    {
      name: "login",
      path: "/login",
      component: () => import("@/views/Login"),
      meta: {
        requiresAuth: false,
        layout: "no-auth-layout",
      },
    },
    {
      name: "signup",
      path: "/signup",
      component: () => import("@/views/registration/RegistrationForm"),
      meta: {
        requiresAuth: false,
        layout: "no-auth-layout",
      },
    },
    {
      name: "registration",
      path: "/registration",
      component: () => import("@/views/registration/RegistrationForm"),
      meta: {
        requiresAuth: false,
        layout: "no-auth-layout",
      },
    },
    {
      name: "confirmation",
      path: "/confirmation",
      component: () => import("@/views/registration/ActivationEmailSent"),
      meta: {
        requiresAuth: false,
        layout: "no-auth-layout",
      },
    },
    {
      name: "activation",
      path: "/activation/:id",
      component: () => import("@/views/registration/ActivationForm"),
      meta: {
        requiresAuth: false,
        requiredBoth: true,
        layout: "no-auth-layout",
      },
    },
    {
      name: "forgot-password",
      path: "/forgot-password",
      component: () => import("@/views/ForgotPassword"),
      meta: {
        requiresAuth: false,
        layout: "no-auth-layout",
      },
    },
    {
      name: "password-recovery",
      path: "/password-recovery/:token",
      component: () => import("@/views/PasswordRecovery"),
      meta: {
        requiresAuth: false,
        layout: "no-auth-layout",
      },
    },
    {
      name: "home",
      path: "/home",
      component: () => import("@/views/Home"),
      meta: {
        requiresAuth: false,
        layout: "app-layout",
      },
    },
    {
      name: "trends-home-post",
      path: "/trends/:id",
      component: () => import("@/components/trends/TrendPost"),
      meta: {
        requiresAuth: true,
        layout: "app-layout",
      },
    },
    {
      name: "trends-home-post-getabstract",
      path: "/trends-getabstract/:id",
      component: () =>
        import("@/components/clients/elements/TrendPostGetAbstract"),
      meta: {
        requiresAuth: true,
        layout: "app-layout",
      },
    },
    {
      name: "dashboard",
      path: "/dashboard",
      component: () => import("@/views/Dashboard"),
      meta: {
        requiresAuth: true,
        layout: "app-layout",
      },
    },

    {
      name: "dashboard-imo",
      path: "/dashboard-imo",
      component: () => import("@/views/DashboardIMO"),
      meta: {
        requiresAuth: true,
        layout: "app-layout",
      },
    },

    {
      name: "news",
      path: "/news",
      component: () => import("@/views/News"),
      meta: {
        requiresAuth: true,
        layout: "app-layout",
      },
    },
    {
      name: "trends",
      path: "/trends",
      component: () => import("@/views/Trends"),
      meta: {
        requiresAuth: true,
        layout: "app-layout",
      },
    },
    {
      name: "my-profile",
      path: "/my-profile",
      component: () => import("@/views/profile/Profile"),
      meta: {
        requiresAuth: true,
        layout: "app-layout",
      },
    },
    {
      name: "getabstract",
      path: "/getabstract",
      component: () => import("@/views/getabstract/GetAbstract"),
      meta: {
        requiresAuth: true,
        layout: "app-layout",
      },
    },
    {
      name: "my-plan",
      path: "/my-plan",
      component: () => import("@/views/plans/Plan"),
      meta: {
        requiresAuth: true,
        layout: "app-layout",
      },
    },
    {
      name: "plans",
      path: "/plans",
      component: () => import("@/views/plans/Plans"),
      meta: {
        requiresAuth: true,
        layout: "app-layout",
      },
    },
    {
      name: "edit-profile",
      path: "/my-profile/edit",
      component: () => import("@/views/profile/EditUserProfile"),
      meta: {
        requiresAuth: true,
        layout: "app-layout",
      },
    },
    {
      name: "change-password",
      path: "/my-profile/change-password",
      component: () => import("@/views/profile/ChangePassword"),
      meta: {
        requiresAuth: true,
        layout: "app-layout",
      },
    },
    {
      name: "complete-profile",
      path: "/complete-profile",
      component: () => import("@/views/profile/CompleteProfile"),
      meta: {
        requiresAuth: true,
        layout: "no-auth-layout",
      },
    },
    {
      name: "subscription",
      path: "/subscription",
      component: () => import("@/views/Subscription"),
      meta: {
        requiresAuth: true,
        layout: "app-layout",
      },
    },
    {
      name: "subscribe",
      path: "/subscribe/:plan",
      component: () => import("@/views/Subscribe"),
      meta: {
        requiresAuth: true,
        layout: "app-layout",
      },
    },
    {
      name: "payu",
      path: "/payu",
      component: () => import("@/views/Subscription/PayUForm"),
      meta: {
        requiresAuth: true,
        layout: "app-layout",
      },
    },
    {
      name: "dti-form",
      path: "/dti-form",
      component: () => import("@/views/forms/Form"),
      meta: {
        requiresAuth: true,
        layout: "app-layout",
      },
    },
    {
      name: "survey",
      path: "/survey",
      component: () => import("@/views/forms/Survey"),
      meta: {
        requiresAuth: true,
        layout: "app-layout",
      },
    },
    {
      name: "survey-imo",
      path: "/survey-imo",
      component: () => import("@/views/survey/SurveyIMO"),
      meta: {
        requiresAuth: true,
        layout: "app-layout",
      },
    },

    {
      name: "dashboardimension",
      path: "/dashboardimension/:id",
      component: () => import("@/views/DashboardDimension"),
      meta: {
        requiresAuth: true,
        layout: "app-layout",
      },
    },
    // ====================================+
    // PAGOS
    // ====================================+
    {
      name: "billing-result",
      path: "/billing/voucher/:id",
      component: () => import("@/views/billing/PaymentResult"),
      meta: {
        requiresAuth: true,
        layout: "no-auth-layout",
      },
    },
    {
      name: "enroll-result",
      path: "/billing/oneclick/enroll/:success",
      component: () => import("@/views/billing/OneClickEnrollResult"),
      meta: {
        requiresAuth: true,
        layout: "no-auth-layout",
      },
    },
    {
      name: "webpayplus-result",
      path: "/billing/webpayplus/:success",
      component: () => import("@/views/billing/WebpayPlusResult"),
      meta: {
        requiresAuth: true,
        layout: "no-auth-layout",
      },
    },
    {
      name: "success-payment",
      path: "/billing/payment/success",
      component: () => import("@/views/billing/SuccessfulPayment"),
      meta: {
        requiresAuth: true,
        layout: "app-layout",
      },
      props: (route) => ({ query: route.query.paymentMethod }),
    },
    {
      name: "success-payment-payu",
      path: "/billing/payment/payu/success",
      component: () => import("@/views/billing/SuccessfulPaymentPayu"),
      meta: {
        requiresAuth: true,
        layout: "app-layout",
      },
      //props: route => ({ paymentData : route.params })
    },
    {
      name: "error-payment",
      path: "/billing/payment/error",
      component: () => import("@/views/billing/ErrorPayment"),
      meta: {
        requiresAuth: true,
        layout: "app-layout",
      },
    },
    {
      name: "my-payment",
      path: "/billing/mypayments/",
      component: () => import("@/views/billing/MyPayments"),
      meta: {
        requiresAuth: true,
        layout: "app-layout",
      },
    },
    // ====================================+
    // Ranking
    // ====================================+
    {
      name: "ranking",
      path: "/ranking",
      component: () => import("@/views/Ranking"),
      meta: {
        requiresAuth: true,
        layout: "app-layout",
      },
    },
    // ====================================+
    // BRIEFCASE(PORTAFOLIO)
    // ====================================+
    {
      name: "briefcase",
      path: "/briefcase",
      component: () => import("@/views/briefcase/MyBriefcase"),
      meta: {
        requiresAuth: true,
        layout: "app-layout",
      },
    },
    {
      name: "evaluation_client",
      path: "/evaluation_client/:id",
      component: () => import("@/views/briefcase/EvaluationClient"),
      meta: {
        requiresAuth: true,
        layout: "no-auth-layout",
      },
    },
    {
      name: "succesful-evaluation",
      path: "/succesful-evaluation",
      component: () => import("@/views/briefcase/SuccesfulEvaluation"),
      meta: {
        requiresAuth: true,
        layout: "no-auth-layout",
      },
    },
    // ====================================+
    // LICITACIÓN
    // ====================================+
    {
      name: "project-foundation",
      path: "/project-foundation",
      component: () => import("@/views/projects/Foundation"),
      meta: {
        requiresAuth: true,
        layout: "app-layout",
      },
    },
    {
      name: "my-projects/created",
      path: "/my-projects/created/:projectId",
      redirect: () => ({ name: "my-projects" }),
    },
    {
      name: "my-projects/request-created",
      path: "/my-projects/request-created/:tenderRequestId",
      redirect: () => ({ name: "my-projects" }),
    },
    {
      name: "my-projects",
      path: "/my-projects",
      component: () => import("@/views/projects/MyProjects"),
      meta: {
        requiresAuth: true,
        layout: "app-layout",
      },
    },
    {
      name: "applications",
      path: "/my-projects/applications/:projectId",
      component: () => import("@/views/projects/Applications"),
      meta: {
        requiresAuth: true,
        layout: "app-layout",
      },
    },
    {
      name: "create-project",
      path: "/create-project/:projectId",
      redirect: "/create-project/:projectId/GeneralData",
      component: () => import("@/views/projects/CreateProject"),
      children: [
        {
          path: "GeneralData",
          component: () => import("@/views/projects/steps/GeneralData"),
          meta: {
            step: 0,
          },
        },
        {
          path: "improvements",
          component: () => import("@/views/projects/steps/Improvements"),
          meta: {
            step: 1,
          },
        },
        {
          path: "Requeriments",
          component: () => import("@/views/projects/steps/Requeriments"),
          meta: {
            step: 2,
          },
        },
        {
          path: "Confirmation",
          component: () => import("@/views/projects/steps/Confirmation"),
          meta: {
            step: 3,
          },
        },
      ],
      meta: {
        requiresAuth: true,
        layout: "app-layout",
      },
    },
    {
      name: "project-presentation",
      path: "/project-presentation/:projectId",
      component: () =>
        import("@/views/specialist-projects/ProjectPresentation"),
      meta: {
        requiresAuth: true,
        layout: "app-layout",
      },
    },
    {
      name: "project-description",
      path: "/project-description/:projectId",
      component: () => import("@/views/specialist-projects/ProjectDescription"),
      meta: {
        requiresAuth: true,
        layout: "app-layout",
      },
    },
    {
      name: "project-detail",
      path: "/project-detail/:projectId",
      component: () => import("@/views/projects/ProjectDetail"),
      meta: {
        requiresAuth: true,
        layout: "app-layout",
      },
    },
    {
      name: "apply-for-project",
      path: "/apply-for-project/:id",
      redirect: "/apply-for-project/:id/general-data",
      component: () => import("@/views/specialist-projects/ApplyForProject"),
      children: [
        {
          path: "general-data",
          component: () =>
            import("@/views/specialist-projects/apply-for-project/GeneralData"),
          meta: {
            step: 0,
          },
        },
        {
          path: "optimization",
          component: () =>
            import(
              "@/views/specialist-projects/apply-for-project/Organization"
            ),
          meta: {
            step: 1,
          },
        },
        {
          path: "team",
          component: () =>
            import("@/views/specialist-projects/apply-for-project/Team"),
          meta: {
            step: 2,
          },
        },
        {
          path: "confirmation",
          component: () =>
            import(
              "@/views/specialist-projects/apply-for-project/Confirmation"
            ),
          meta: {
            step: 3,
          },
        },
      ],
      meta: {
        requiresAuth: true,
        layout: "app-layout",
      },
    },
    {
      name: "project-tracking",
      path: "/project-tracking/:projectId",
      component: () => import("@/views/specialist-projects/ProjectTracking"),
      meta: {
        requiresAuth: true,
        layout: "app-layout",
      },
    },
    // ====================================+
    // PMO DIGITAL
    // ====================================+
    {
      name: "pmo-digital",
      path: "/pmo-digital/:projectId",
      component: () => import("@/views/pmo/PmoDigital"),
      meta: {
        requiresAuth: false,
        layout: "app-layout",
      },
    },
    {
      name: "pmo-digital-budget",
      path: "/pmo-digital/budget",
      component: () => import("@/views/pmo/Budget"),
      meta: {
        requiresAuth: false,
        layout: "app-layout",
      },
    },
    {
      name: "pmo-digital-budget-configuration",
      path: "/pmo-digital/configuration",
      component: () => import("@/views/pmo/Configuration"),
      meta: {
        requiresAuth: false,
        layout: "app-layout",
      },
    },
    {
      name: "pmo-digital-ficha",
      path: "/pmo-digital/ficha",
      component: () => import("@/views/pmo/Ficha"),
      meta: {
        requiresAuth: false,
        layout: "app-layout",
      },
    },
    {
      name: "pmo-digital-planification",
      path: "/pmo-digital/planification",
      component: () => import("@/views/pmo/Planification"),
      meta: {
        requiresAuth: false,
        layout: "app-layout",
      },
    },
    {
      name: "pmo-digital-budget-seguimiento",
      path: "/pmo-digital/seguimiento",
      component: () => import("@/views/pmo/Seguimiento"),
      meta: {
        requiresAuth: false,
        layout: "app-layout",
      },
    },
    {
      name: "pmo-digital-risk",
      path: "/pmo-digital/risk",
      component: () => import("@/views/pmo/Risk"),
      meta: {
        requiresAuth: false,
        layout: "app-layout",
      },
    },
    {
      name: "pmo-digital-objectives",
      path: "/pmo-digital/objectives",
      component: () => import("@/views/pmo/Objectives"),
      meta: {
        requiresAuth: false,
        layout: "app-layout",
      },
    },
    {
      name: "pmo-digital-kpibusiness",
      path: "/pmo-digital/kpibusiness",
      component: () => import("@/views/pmo/KPIBusiness"),
      meta: {
        requiresAuth: false,
        layout: "app-layout",
      },
    },
    {
      name: "pmo-digital-organizational-agility",
      path: "/pmo-digital/organizational-agility",
      component: () => import("@/views/pmo/OrganizationalAgility"),
      meta: {
        requiresAuth: false,
        layout: "app-layout",
      },
    },
    {
      name: "pmo-digital-engagement",
      path: "/pmo-digital/engagement",
      component: () => import("@/views/pmo/Engagement"),
      meta: {
        requiresAuth: false,
        layout: "app-layout",
      },
    },
    // ====================================+
    // ERRORS
    // ====================================+
    {
      name: "404",
      path: "*",
      component: () => import("@/views/errors/404"),
      meta: {
        requiredBoth: true,
        layout: "no-auth-layout",
        metaTags: [
          {
            name: "robots",
            content: "noindex",
          },
        ],
      },
    },
  ],
});
