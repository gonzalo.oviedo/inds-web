import * as d3 from "d3";

export default function render(element, { data, width }) {
  let radius = width / 6;

  // Create div for tooltip
  // class 'v-tooltip_content' is used from
  // the Vuetify component.
  const tooltip = d3
    .select("body")
    .append("div")
    .attr("class", "v-tooltip__content")
    .style("z-index", 2000)
    .style("opacity", 0);

  const color = d3.scaleOrdinal(
    d3.quantize(d3.interpolate("#7319D5", "white"), data.children.length + 1)
  );

  const partition = (data) => {
    const root = d3
      .hierarchy(data)
      .sum((d) => d.value)
      .sort((a, b) => b.value - a.value);
    return d3.partition().size([2 * Math.PI, root.height + 1])(root);
  };

  const arc = d3
    .arc()
    .startAngle((d) => d.x0)
    .endAngle((d) => d.x1)
    .padAngle((d) => Math.min((d.x1 - d.x0) / 2, 0.005))
    .padRadius(radius * 1.5)
    .innerRadius((d) => d.y0 * radius)
    .outerRadius((d) => Math.max(d.y0 * radius, d.y1 * radius - 1));

  let root = partition(data);

  root.each((d) => (d.current = d));

  const svg = d3
    .select(element)
    .append("svg")
    .attr("viewBox", [0, 0, width, width]);

  const g = svg
    .append("g")
    .attr("transform", `translate(${width / 2},${width / 2})`);

  const path = g
    .append("g")
    .selectAll("path")
    .data(root.descendants().slice(1))
    .join("path")
    .attr("fill", (d) => {
      while (d.depth > 1) d = d.parent;
      return color(d.data.name);
    })
    .attr("fill-opacity", (d) =>
      arcVisible(d.current) ? (d.children ? 1 : 0.8) : 0
    )
    .attr("d", (d) => arc(d.current))
    .on("mouseover", function (evt, d) {
      tooltip.transition().duration(200).style("opacity", 0.9);
      tooltip
        .html(d.data.name)
        .style("left", `${evt.pageX}px`)
        .style("top", `${evt.pageY - 28}px`);
    })
    .on("mouseout", () => {
      tooltip.transition().duration(200).style("opacity", 0);
    });

  path
    .filter((d) => d.children)
    .style("cursor", "pointer")
    .on("click", clicked);

  const label = g
    .append("g")
    .attr("pointer-events", "none")
    .attr("text-anchor", "middle")
    .style("user-select", "none")
    .selectAll("text")
    .data(root.descendants().slice(1))
    .join("text")
    .attr("dy", "0.35em")
    .attr("fill-opacity", (d) => +labelVisible(d.current))
    .attr("transform", (d) => labelTransform(d.current))
    .attr("fill", "white")
    .style("font-weight", "normal")
    .style("font-family", "Roboto Condensed")
    .style("font-size", "9px")
    .text((d) => d.data.name)
    .call(wrap);

  function wrap(t) {
    t.each(function (d) {
      let arcWidth =
        (Math.max(d.y0 * radius, d.y1 * radius - 1) - d.y0 * radius) * 0.8;
      let currentWidth = this.getComputedTextLength();

      let textNode = d3.select(this);
      let arcText = d.data.name;

      if (currentWidth > arcWidth) {
        while (currentWidth > arcWidth) {
          arcText = arcText.slice(0, -1);
          textNode = textNode.text(arcText);
          currentWidth = textNode.node().getComputedTextLength();
        }
        textNode.text(`${arcText}...`);
      }
    });
  }

  const parent = g
    .append("image")
    .datum(root)
    .attr("class", "hide-icon")
    .attr("href", require("@/assets/img/refresh-icon.svg"))
    .style("cursor", "pointer")
    .attr("width", "30px")
    .attr("height", "30px")
    .attr("x", "-15px")
    .attr("y", "-15px")
    .on("click", clicked);

  function clicked(event, p) {
    parent.datum(p.parent || root);

    let iconClass = p.depth > 0 ? "show-icon" : "hide-icon";
    parent.attr("class", iconClass);

    root.each(
      (d) =>
        (d.target = {
          x0:
            Math.max(0, Math.min(1, (d.x0 - p.x0) / (p.x1 - p.x0))) *
            2 *
            Math.PI,
          x1:
            Math.max(0, Math.min(1, (d.x1 - p.x0) / (p.x1 - p.x0))) *
            2 *
            Math.PI,
          y0: Math.max(0, d.y0 - p.depth),
          y1: Math.max(0, d.y1 - p.depth),
        })
    );

    const t = g.transition().duration(750);

    // Transition the data on all arcs, even the ones that aren’t visible,
    // so that if this transition is interrupted, entering arcs will start
    // the next transition from the desired position.
    path
      .transition(t)
      .tween("data", (d) => {
        const i = d3.interpolate(d.current, d.target);
        return (t) => (d.current = i(t));
      })
      .filter(function (d) {
        return +this.getAttribute("fill-opacity") || arcVisible(d.target);
      })
      .attr("fill-opacity", (d) =>
        arcVisible(d.target) ? (d.children ? 1 : 0.8) : 0
      )
      .attrTween("d", (d) => () => arc(d.current));

    label
      .filter(function (d) {
        return +this.getAttribute("fill-opacity") || labelVisible(d.target);
      })
      .transition(t)
      .attr("fill-opacity", (d) => +labelVisible(d.target))
      .attrTween("transform", (d) => () => labelTransform(d.current));
  }

  function arcVisible(d) {
    return d.y1 <= 3 && d.y0 >= 1 && d.x1 > d.x0;
  }

  function labelVisible(d) {
    return d.y1 <= 3 && d.y0 >= 1 && (d.y1 - d.y0) * (d.x1 - d.x0) > 0.03;
  }

  function labelTransform(d) {
    const x = (((d.x0 + d.x1) / 2) * 180) / Math.PI;
    const y = ((d.y0 + d.y1) / 2) * radius;
    return `rotate(${x - 90}) translate(${y},0) rotate(${x < 180 ? 0 : 180})`;
  }

  return svg.node();
}
