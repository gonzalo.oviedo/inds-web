import companyService from "../../services/companyService";

const CompanyStore = {
  namespaced: true,
  state: {
    company: {},
  },
  getters: {
    clientType: (state) => state.company.type,
  },
  actions: {
    init({ commit }) {
      commit("INITIALIZE_COMPANY");
    },
    async companyProfile({ commit }) {
      let company = await companyService.getCompanyProfile();
      commit("ADD_COMPANY", company);
    },
  },
  mutations: {
    INITIALIZE_COMPANY(state) {
      state.company = {};
    },
    ADD_COMPANY(state, company) {
      state.company = company;
    },
  },
};

export default CompanyStore;
