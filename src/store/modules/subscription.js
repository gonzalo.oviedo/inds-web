import Vue from "vue";
import subscriptionService from "@/services/subscriptionService";

const state = {
  id: "",
  period: "",
  paymentMethod: "",
  plan: {},
  subscriptionDate: "",
  payments: [],
  status: "",
  lastUpdate: "",
  lastSubscription: "",
};

const getters = {
  getSubscription(state) {
    return {
      id: state.id,
      period: state.period,
      paymentMethod: state.paymentMethod,
      plan: state.plan,
      subscriptionDate: state.subscriptionDate,
      payments: state.payments,
      status: state.status,
      lastUpdate: state.lastUpdate,
    };
  },
  getLastUpdate(state) {
    return state.lastUpdate;
  },
  getValidateSubscription(state) {
    if (state.lastSubscription) {
      if (
        new Date(state.lastSubscription).getTime() >
          new Date(2021, 4, 3).getTime() ||
        state.plan.name == "BUSINESS"
      ) {
        return false;
      } else {
        return true;
      }
    } else {
      return true;
    }
  },
};

const actions = {
  setSubscription({ commit }, payload) {
    commit("SET_SUBSCRIPTION", payload);
  },

  getSubscription({ commit }) {
    subscriptionService
      .getCurrentUserSubscription()
      .then((subscription) => {
        commit("SET_SUBSCRIPTION", subscription);
      })
      .catch((err) => Vue.$log.error(err));
  },
};

const mutations = {
  SET_SUBSCRIPTION(state, payload) {
    state.id = payload.id;
    state.period = payload.period;
    state.paymentMethod = payload.paymentMethod;
    state.plan = payload.plan;
    state.subscriptionDate = payload.subscriptionDate;
    state.payments = payload.payments;
    if (payload.payments.length > 0) {
      state.lastSubscription = payload.payments[0].paymentDate;
    }
    state.status = payload.status;
    state.lastUpdate = payload.lastUpdate;

    localStorage.setItem("subscription", payload.id);
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
