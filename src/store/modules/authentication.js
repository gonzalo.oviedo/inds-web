import Vue from "vue";
import LoginService from "@/services/loginService";
import CurrentUserService from "@/services/currentUserService";
import SubscriptionService from "@/services/subscriptionService";
import CompanyService from "@/services/companyService";

const state = {
  authenticated: false,
  userInfo: {
    principal: {},
  },
  userName: "",
  authInfo: {},
  accessToken: localStorage.getItem("accessToken") || "",
};

const getters = {
  hasUser(state) {
    return Object.keys(state.authInfo).length != 0;
  },
  isAuth(state) {
    return Object.values(state.accessToken).length != 0;
  },
  Auth(state) {
    return state.accessToken;
  },
};

const actions = {
  doLogin({ commit }, credentials) {
    commit("CLEAN_LOCAL_STORAGE");
    return new Promise((resolve, reject) => {
      commit("SET_AUTHENTICATION_STATUS", false);
      LoginService.login(
        Vue._.get(credentials, "username"),
        Vue._.get(credentials, "password")
      ).then((authInfo) => {
        if (Vue._.get(authInfo, "isAxiosError")) {
          commit("SET_AUTHENTICATION_STATUS", false);
          reject(false);
        } else {
          commit("SET_AUTHENTICATION_STATUS", true);
          commit("SET_AUTH_INFO", authInfo);
          commit("SET_ACCESS_TOKEN", Vue._.get(authInfo, "access_token"));

          CurrentUserService.currentUser(Vue._.get(authInfo, "access_token"))
            .then((userInfo) => {
              commit("SET_USER_INFO", userInfo);
              SubscriptionService.getCurrentUserSubscription();
              CompanyService.getCompanyProfile()
                .then((company) => {
                  if (Vue._.has(company, "emptyProfile")) {
                    resolve({ name: "complete-profile" });
                    return;
                  }
                  commit("SET_CLIENT_INFO", company);
                  resolve({ name: "home" });
                })
                .catch((error) => reject(error));
            })
            .catch((error) => reject(error));
        }
      });
    });
  },
  doLogout: function ({ commit }) {
    return new Promise((resolve) => {
      commit("LOGOUT");
      resolve();
    });
  },
  doRegister: function ({ commit }, credentials) {
    commit("CLEAN_LOCAL_STORAGE");
    localStorage.setItem("userName", credentials.firstName);
    return new Promise((resolve, reject) => {
      commit("SET_AUTHENTICATION_STATUS", false);
      LoginService.register(
        Vue._.get(credentials, "firstName"),
        Vue._.get(credentials, "lastName"),
        Vue._.get(credentials, "companyName"),
        Vue._.get(credentials, "companyIdentifier"),
        Vue._.get(credentials, "email"),
        Vue._.get(credentials, "clientType"),
        Vue._.get(credentials, "countryCode"),
        Vue._.get(credentials, "source"),
        Vue._.get(credentials, "token"),
        Vue._.get(credentials, "businessParams"),
        Vue._.get(credentials, "promo")
      ).then((authInfo) => {
        if (Vue._.get(authInfo, "isAxiosError")) {
          commit("SET_AUTHENTICATION_STATUS", false);
          reject();
        }
        resolve(true);
      });
    });
  },
  doActivate: function ({ commit }, credentials) {
    commit("CLEAN_LOCAL_STORAGE");
    return new Promise((resolve, reject) => {
      commit("SET_AUTHENTICATION_STATUS", false);
      LoginService.activate(
        Vue._.get(credentials, "userId"),
        Vue._.get(credentials, "password")
      ).then((authInfo) => {
        if (Vue._.get(authInfo, "isAxiosError")) {
          commit("SET_AUTHENTICATION_STATUS", false);
          reject();
        }
      });
    });
  },
  validateToken: function ({ commit }, token) {
    return new Promise((resolve) => {
      LoginService.validateToken(token)
        .then(() => {
          commit("SET_AUTHENTICATION_STATUS", true);
          resolve(true);
        })
        .catch(() => resolve(false));
    });
  },
  isAuthenticated: function ({ state }) {
    return new Promise((resolve, reject) => {
      if (!Vue._.get(state, "accessToken")) {
        reject({ message: "Null or Empty token" });
      } else {
        Vue.$log.debug("AccessToken: ", Vue._.get(state, "accessToken"));
        LoginService.validateToken(Vue._.get(state, "accessToken"))
          .then((response) => {
            if (response) {
              resolve(true);
            } else {
              reject({ message: "Invalid token" });
              this.doLogout();
            }
          })
          .catch(() => reject({ message: "Unexpected networking error" }));
      }
    });
  },
};

const mutations = {
  SET_AUTHENTICATION_STATUS(state, status) {
    state.authenticated = status;
  },
  SET_AUTH_INFO(state, authInfo) {
    state.authInfo = authInfo;
    localStorage.setItem("authInfo", JSON.stringify(authInfo));
  },
  SET_USER_INFO(state, userInfo) {
    state.userInfo = userInfo;
    localStorage.setItem("userInfo", JSON.stringify(userInfo));
    localStorage.setItem("userName", userInfo.name);
    localStorage.setItem("userId", userInfo.principal.id);
  },
  SET_ACCESS_TOKEN(state, accessToken) {
    state.accessToken = accessToken;
    localStorage.setItem("accessToken", accessToken);
  },
  SET_CLIENT_INFO(state, clientInfo) {
    state.clientInfo = clientInfo;
    localStorage.setItem("clientInfo", JSON.stringify(clientInfo));
  },
  LOGOUT(state) {
    state.accessToken = "";
    state.authInfo = {};
    state.userInfo = {
      principal: {},
    };
    (state.userName = ""), (state.accessToken = "");
    state.authenticated = false;

    localStorage.removeItem("authInfo");
    localStorage.removeItem("previus");
    localStorage.removeItem("accessToken");
    localStorage.removeItem("userInfo");
    localStorage.removeItem("clientInfo");
    localStorage.removeItem("formMixin");
    localStorage.removeItem("userId");
    localStorage.removeItem("userName");
    localStorage.removeItem("payment");
    localStorage.removeItem("subscription");
    localStorage.removeItem("answers");
    localStorage.removeItem("percentage");
    localStorage.removeItem("answersImo");
    localStorage.removeItem("percentageImo");
  },

  CLEAN_LOCAL_STORAGE() {
    localStorage.removeItem("authInfo");
    localStorage.removeItem("accessToken");
    localStorage.removeItem("userInfo");
    localStorage.removeItem("clientInfo");
    localStorage.removeItem("formMixin");
    localStorage.removeItem("userId");
    localStorage.removeItem("userName");
    localStorage.removeItem("payment");
    localStorage.removeItem("subscription");
  },
};

export default {
  namespaced: true,
  actions,
  mutations,
  state,
  getters,
};
