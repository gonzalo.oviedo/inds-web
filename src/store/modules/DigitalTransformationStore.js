import DtService from "../../services/dtService";

const DigitalTransformationStore = {
  namespaced: true,
  state: {
    questions: {},
    percentageImo: 0,
  },
  getters: {
    answered: (state) => state.questions.answered || 0,
    total: (state) => state.questions.total || 0,
    percentage: (state) => {
      if (state.total === 0) {
        return 0;
      }
      return Math.floor(
        (state.questions.answered / state.questions.total) * 100
      );
    },
    percentageImo: (state) => state.percentageImo,
  },
  actions: {
    init({ commit }) {
      commit("INITIALIZE_DT");
    },
    async surveyComplete({ commit }, userId) {
      let questions = await DtService.getSurveyComplete("a123", userId);
      let percentageImo = await DtService.getSurveyComplete(
        "imo-index-v1",
        userId
      ).then((data) => {
        return Math.floor((data.answered / data.total) * 100);
      });

      commit("ADD_QUESTIONS", questions);
      commit("ADD_PERCENTAGE", percentageImo);
    },
  },
  mutations: {
    INITIALIZE_DT(state) {
      state.questions = {};
      state.percentageImo = null;
    },
    ADD_QUESTIONS(state, questions) {
      state.questions = questions;
    },
    ADD_PERCENTAGE(state, percentageImo) {
      state.percentageImo = percentageImo;
    },
  },
};

export default DigitalTransformationStore;
