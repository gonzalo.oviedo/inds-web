import currentUserService from "../../services/currentUserService";

const CurrentUserStore = {
  namespaced: true,
  state: {
    currentUser: {},
  },
  getters: {
    currentUser: (state) => state.currentUser,
    userId: (state) => state.currentUser.id,
  },
  actions: {
    init({ commit }) {
      commit("INITIALIZE_CURRENT_USER");
    },
    async currentUser({ commit }) {
      let userData = await currentUserService.currentUserData();
      commit("SET_CURRENT_USER", userData);
    },
  },
  mutations: {
    INITIALIZE_CURRENT_USER(state) {
      state.currentUser = {};
    },
    SET_CURRENT_USER(state, userData) {
      state.currentUser = userData;
    },
  },
};

export default CurrentUserStore;
