import CurrentUserService from "@/services/currentUserService";
import PlanService from "@/services/planService";
import LoginService from "@/services/loginService";
import Vue from "vue";

const UserStore = {
  namespaced: true,
  getters: {
    getUser: (state) => state.user,
    getSuscription: (state) => state.suscription,
    getPercentage: (state) => state.percentage,
    getAnswers: (state) => state.answers,
    getImtd: (state) => state.imtd,
    getSteps: (state) => state.steps,
  },
  state: {
    user: {
      accountNonExpired: null,
      accountNonLocked: null,
      applications: null,
      authorities: null,
      credentialsNonExpired: null,
      enabled: null,
      firstName: null,
      id: null,
      lastName: null,
      updatedOn: null,
      username: null,
    },
    suscription: {
      plan: {
        name: "",
      },
    },
    percentage: localStorage.getItem("percentage") || 0,
    answers: JSON.parse(localStorage.getItem("answers")) || {},
    steps: [
      {
        title: "Realiza la encuesta de Transformación Digital",
        redirectTo: { name: "dti-form" },
        active: false,
      },
      {
        title: "Completa tu perfil al 100%",
        redirectTo: { name: "my-profile" },
        active: false,
      },
      {
        title:
          "Suscríbete a un plan y obtén en detalle tu diagnóstico en Transformación Digital",
        redirectTo: { name: "my-plan" },
        active: false,
      },
      {
        title:
          "Conoce los contenidos y tendencias más importantes sobre Transformación",
        redirectTo: { name: "trends" },
        active: false,
      },
    ],
    imtd: null,
  },

  actions: {
    SET_PERCENTAGE({ commit }, percentage) {
      commit("CLEAN_LOCAL_STORAGE_PERCENTAGE");
      commit("SET_PERCENTAGE", percentage);
    },
    SET_IMTD({ commit }, imtd) {
      commit("SET_IMTD", imtd);
    },
    SET_ANSWERS({ commit }, answers) {
      commit("CLEAN_LOCAL_STORAGE_ANSWERS");
      commit("SET_ANSWERS", answers);
    },
    SET_STEP_ACTIVE({ commit }, index) {
      commit("SET_STEP_ACTIVE", index);
    },
    SET_STEP_INACTIVE({ commit }, index) {
      commit("SET_STEP_INACTIVE", index);
    },
    FETCH_USER({ commit }, state) {
      commit("CLEAN_LOCAL_STORAGE");
      new Promise((resolve, reject) => {
        if (!Vue._.get(state, "accessToken")) {
          console.log("no auth");
          resolve("No auth");
        } else {
          Vue.$log.debug("AccessToken: ", Vue._.get(state, "accessToken"));
          LoginService.validateToken(Vue._.get(state, "accessToken"))
            .then((response) => {
              if (response) {
                new Promise((resolve, reject) => {
                  CurrentUserService.currentUserData()
                    .then((response) => {
                      commit("SET_USER", response);
                      resolve(response);
                    })
                    .catch((error) => reject(error));
                });
                new Promise((resolve, reject) => {
                  PlanService.getSuscription()
                    .then((response) => {
                      commit("SET_SUSCRIPTION", response);
                      resolve(response);
                    })
                    .catch((error) => reject(error));
                });
              } else {
                reject({ message: "Invalid token" });
              }
            })
            .catch(() => reject({ message: "Unexpected networking error" }));
        }
      });
    },
  },
  mutations: {
    SET_USER(state, user) {
      state.user = user;
      localStorage.setItem("user", JSON.stringify(user));
    },
    SET_PERCENTAGE(state, percentage) {
      state.percentage = percentage;
      localStorage.setItem("percentage", JSON.stringify(percentage));
    },
    SET_ANSWERS(state, answers) {
      state.answers = answers;
      localStorage.setItem("answers", JSON.stringify(answers));
    },
    SET_SUSCRIPTION(state, suscription) {
      state.suscription = suscription;
      localStorage.setItem("suscription", JSON.stringify(suscription));
    },
    CLEAN_LOCAL_STORAGE_PERCENTAGE() {
      localStorage.removeItem("percentage");
    },
    CLEAN_LOCAL_STORAGE_ANSWERS() {
      localStorage.removeItem("answers");
    },
    CLEAN_LOCAL_STORAGE() {
      localStorage.removeItem("user");
      localStorage.removeItem("suscription");
    },
    SET_IMTD(state, imtd) {
      state.imtd = imtd;
    },
    SET_STEP_ACTIVE(state, index) {
      state.steps[index].active = true;
    },
    SET_STEP_INACTIVE(state, index) {
      state.steps[index].active = false;
    },
  },
};

export default UserStore;
