import Vue from "vue";
import planService from "@/services/planService";

const state = {
  plans: {},
  suscriptions: {},
};
const getters = {
  getPlans(state) {
    return state.plans;
  },
  getSuscriptions(state) {
    return state.suscriptions;
  },
};
const actions = {
  setPlans({ commit }) {
    planService
      .getPlans()
      .then((response) => {
        commit("SET_PLANS", response);
      })
      .catch((err) => Vue.$log.error(err));
  },
  setSuscriptions({ commit }) {
    planService
      .getSuscription()
      .then((response) => {
        commit("SET_SUSCRIPTIONS", response);
      })
      .catch((err) => Vue.$log.error(err));
  },
};
const mutations = {
  SET_PLANS(state, data) {
    state.plans = data;
  },
  SET_SUSCRIPTIONS(state, data) {
    state.suscriptions = data;
  },
};
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
