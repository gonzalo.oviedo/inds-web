const state = {
  rankingflag1: localStorage.getItem("rankingflag1") ? true : false,
  rankingflag2: localStorage.getItem("rankingflag2") ? true : false,
  rankingflag3: localStorage.getItem("rankingflag3") ? true : false,
};

const getters = {
  getflag1(state) {
    return state.rankingflag1;
  },
  getflag2(state) {
    return state.rankingflag2;
  },
  getflag3(state) {
    return state.rankingflag3;
  },
};

const actions = {
  setranking({ commit }, flag) {
    commit("SET_RANKING", flag);
  },
  removecache: function ({ commit }) {
    commit("REMOVE_CACHE");
  },
};

const mutations = {
  SET_RANKING(state, ranking) {
    if (ranking == 1) {
      state.rankingflag1 = true;
      localStorage.setItem("rankingflag1", JSON.stringify(true));
    }
    if (ranking == 2) {
      state.rankingflag2 = true;
      localStorage.setItem("rankingflag2", JSON.stringify(true));
    }
    if (ranking == 3) {
      state.rankingflag3 = true;
      localStorage.setItem("rankingflag3", JSON.stringify(true));
    }
  },
  REMOVE_CACHE(state) {
    localStorage.removeItem("rankingflag1");
    localStorage.removeItem("rankingflag2");
    localStorage.removeItem("rankingflag3");
    state.rankingflag1 = false;
    state.rankingflag2 = false;
    state.rankingflag3 = false;
  },
};

export default {
  namespaced: true,
  actions,
  mutations,
  state,
  getters,
};
