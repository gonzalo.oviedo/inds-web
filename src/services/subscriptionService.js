import Vue from "vue";
import store from "@/store";
import uaaAxios from "@/utils/authAxios";

export default {
  getCurrentUserSubscription: function () {
    return new Promise((resolve, reject) => {
      uaaAxios
        .get(`${process.env.VUE_APP_PLANS_API}/subscriptions`)
        .then((response) => {
          let subscription = Vue._.get(response, "data");
          let subscriptionStore = subscription;
          subscriptionStore.payments.sort((a, b) =>
            b.paymentDate > a.paymentDate ? 1 : -1
          );
          store.dispatch("subscription/setSubscription", subscriptionStore);
          resolve(subscription);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },

  addPayment: function (subscriptionId, payment) {
    return new Promise((resolve, reject) => {
      uaaAxios
        .put(
          `${process.env.VUE_APP_PLANS_API}/subscriptions/${subscriptionId}`,
          payment
        )
        .then((response) => {
          let subscription = Vue._.get(response, "data");
          let subscriptionStore = subscription;
          subscriptionStore.payments.sort((a, b) =>
            b.paymentDate > a.paymentDate ? 1 : -1
          );
          store.dispatch("subscription/setSubscription", subscriptionStore);
          resolve(subscription);
        })
        .catch((err) => reject(err));
    });
  },

  subscribe: function (transactionParams) {
    const subscription = Object.assign(transactionParams);
    return new Promise((resolve, reject) => {
      uaaAxios
        .post(`${process.env.VUE_APP_PLANS_API}/subscriptions/`, subscription)
        .then((response) => {
          let data = Vue._.get(response, "data");
          resolve(data);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },

  getPdf: function (pdf) {
    return new Promise((resolve, reject) => {
      uaaAxios({
        url: `plans-api/subscriptions/pdf/${pdf}`,
        method: "GET",
        responseType: "blob",
      })
        .then((response) => {
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },

  payPayu: function (payData, id, paymentType) {
    let payPath =
      paymentType === "ONE_PAYMENT" ? "pay" : "recurrent-payment-pay";
    return new Promise((resolve, reject) => {
      uaaAxios
        .post(
          `${process.env.VUE_APP_PLANS_API}/subscriptions/${id}/${payPath}`,
          payData
        )
        .then((response) => {
          let data = Vue._.get(response, "data");
          resolve(data);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
};
