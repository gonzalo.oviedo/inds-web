import Vue from "vue";
import uaaAxios from "@/utils/authAxios";
import qs from "qs";

const TBK_WEBPAYPLUS_API = `${process.env.VUE_APP_API_URL_BASE}${process.env.VUE_APP_TBK_API}/webpayplus`;

const TO_TRANSBANK_URL = `${process.env.VUE_APP_BASE_URL}/billing/to-transbank?`;

export default {
  // findById
  getTransaction: function (transactionId) {
    let userId = localStorage.getItem("userId");

    return new Promise((resolve, reject) => {
      uaaAxios
        .get(
          `${TBK_WEBPAYPLUS_API}/users/${userId}/transactions/${transactionId}`
        )
        .then((response) => resolve(Vue._.get(response, "data")))
        .catch((err) => reject(err));
    });
  },

  // findAll
  getTransactions: function () {
    let userId = localStorage.getItem("userId");

    return new Promise((resolve, reject) => {
      uaaAxios
        .get(`${TBK_WEBPAYPLUS_API}/users/${userId}/transactions`)
        .then((response) => resolve(Vue._.get(response, "data")))
        .catch((err) => reject(err));
    });
  },

  // getTransactionForm
  getTransactionForm: function (transactionParams) {
    let userId = localStorage.getItem("userId");
    let userName = localStorage.getItem("userName");

    const defaultTransaction = {
      userId: userId,
      email: userName,
      username: userName,
      sessionId: localStorage.getItem("accessToken"),
      successUrl: `${process.env.VUE_APP_BASE_URL}/billing/SuccessfulPayment`,
      failureUrl: `${process.env.VUE_APP_BASE_URL}/billing/ErrorPayment`,
    };

    const transaction = Object.assign(defaultTransaction, transactionParams);

    return new Promise((resolve, reject) => {
      uaaAxios
        .post(`${TBK_WEBPAYPLUS_API}/`, transaction)
        .then((response) => {
          let data = Vue._.get(response, "data");

          if (
            Vue._.get(data, "url") === undefined ||
            Vue._.get(data, "token") === undefined
          ) {
            reject("No ha sido posible generar la url de pago de Transbank.");
            return;
          }

          let url =
            TO_TRANSBANK_URL +
            qs.stringify({
              action: data.url,
              tokenName: "token_ws",
              tokenValue: data.token,
            });

          resolve(url);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
};
