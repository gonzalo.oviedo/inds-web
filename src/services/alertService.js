import Vue from "vue";
import uaaAxios from "@/utils/authAxios";

export default {
  getNotifications: function () {
    return new Promise((resolve, reject) => {
      uaaAxios
        .get(`${process.env.VUE_APP_ALERT_API}/alerts`)
        .then((response) => {
          let data = Vue._.get(response, "data");
          resolve(data);
        })
        .catch((err) => reject(err));
    });
  },

  deleteNotification: function (id) {
    return new Promise((resolve, reject) => {
      uaaAxios
        .delete(`${process.env.VUE_APP_ALERT_API}/alerts?alertId=` + id)
        .then((response) => {
          let data = Vue._.get(response, "data");
          resolve(data);
        })
        .catch((err) => reject(err));
    });
  },

  readNotification: function (id) {
    return new Promise((resolve, reject) => {
      uaaAxios
        .put(`${process.env.VUE_APP_ALERT_API}/alerts?alertId=` + id)
        .then((response) => {
          let data = Vue._.get(response, "data");
          resolve(data);
        })
        .catch((err) => reject(err));
    });
  },
};
