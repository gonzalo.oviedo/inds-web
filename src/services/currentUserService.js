import Vue from "vue";
import uaaAxios from "@/utils/authAxios";

export default {
  /**
   * Funcion para obtener el usuario autenticado.
   *
   * @return user
   */
  currentUser: async () => {
    let response = await uaaAxios.get("/uaa/users/current");
    return response.data;
  },

  /**
   * Servicio par obtener el usuario a partir de la informacion del localStorage.
   *
   * @returns currentUser
   */
  currentUserData: async () => {
    let userId = localStorage.getItem("userId");
    let response = await uaaAxios.get(`/umapi/users/${userId}`);
    return response.data;
  },

  updateUserData: function (user) {
    return new Promise((resolve, reject) => {
      let userId = localStorage.getItem("userId");

      uaaAxios
        .put(
          `${
            process.env.VUE_APP_API_URL_BASE + process.env.VUE_APP_UMAPI_API
          }/users/${userId}`,
          user
        )
        .then((response) => resolve(Vue._.get(response, "data")))
        .catch((err) => reject(err));
    });
  },

  changePassword: function (passwords) {
    return new Promise((resolve, reject) => {
      let userId = localStorage.getItem("userId");

      uaaAxios
        .post(
          `${
            process.env.VUE_APP_API_URL_BASE + process.env.VUE_APP_UMAPI_API
          }/users/${userId}/password`,
          passwords
        )
        .then((response) => resolve(Vue._.get(response, "data")))
        .catch((err) => reject(err));
    });
  },
};
