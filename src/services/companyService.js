import Vue from "vue";
import uaaAxios from "@/utils/authAxios";

export default {
  getCompanyProfile: function () {
    let userInfo = JSON.parse(localStorage.getItem("userInfo"));
    let clientId = "";
    if (userInfo && userInfo.principal && userInfo.principal.clientId) {
      clientId = userInfo.principal.clientId;
    }

    return new Promise((resolve, reject) => {
      if (clientId) {
        uaaAxios
          .get(`${process.env.VUE_APP_COMPANIES_API}/companies/${clientId}`)
          .then((response) => {
            let status = response.status;
            let data = response.data;

            // 201 == CREATED
            if (status == 201) {
              data["emptyProfile"] = true;
            }

            resolve(data);
          })
          .catch((err) => reject(err));
      } else {
        resolve();
      }
    });
  },

  getCompanyProfilePublic: function (id) {
    return new Promise((resolve, reject) => {
      if (id) {
        uaaAxios
          .get(`${process.env.VUE_APP_COMPANIES_API}/specialists/find/${id}`)
          .then((response) => {
            let data = Vue._.get(response, "data");
            resolve(data);
          })
          .catch((err) => reject(err));
      } else {
        resolve();
      }
    });
  },

  getCompanyPublicInfo: function (id) {
    return new Promise((resolve, reject) => {
      uaaAxios
        .get(
          `${process.env.VUE_APP_COMPANIES_API}/companies/project-postulation/${id}`
        )
        .then((response) => {
          let data = Vue._.get(response, "data");
          resolve(data);
        })
        .catch((err) => reject(err));
    });
  },

  updateCompanyProfile: function (data) {
    let userInfo = JSON.parse(localStorage.getItem("userInfo"));
    let clientId = userInfo.principal.clientId;

    return new Promise((resolve, reject) => {
      this.getCompanyProfile()
        .then((company) => {
          for (const prop in data) {
            company[prop] = data[prop];
          }

          let clientType =
            company.type === "CLIENT" ? "clients" : "specialists";

          uaaAxios
            .put(
              `${process.env.VUE_APP_COMPANIES_API}/${clientType}/${clientId}`,
              company
            )
            .then((response) => resolve(Vue._.get(response, "data")))
            .catch((err) => reject(err));
        })
        .catch((err) => reject(err));
    });
  },

  getCategories: function (userId) {
    return new Promise((resolve, reject) => {
      uaaAxios
        .get(
          `${process.env.VUE_APP_COMPANIES_API}/categories?clientId=${userId}`
        )
        .then((response) => {
          resolve(Vue._.get(response, "data"));
        })
        .catch((err) => reject(err));
    });
  },

  getServices: function (userId) {
    return new Promise((resolve, reject) => {
      uaaAxios
        .get(
          `${process.env.VUE_APP_COMPANIES_API}/categories/services?clientId=${userId}`
        )
        .then((response) => {
          resolve(Vue._.get(response, "data"));
        })
        .catch((err) => reject(err));
    });
  },

  getDimensions: function (userId) {
    return new Promise((resolve, reject) => {
      uaaAxios
        .get(
          `${process.env.VUE_APP_COMPANIES_API}/dimensions?clientId=${userId}`
        )
        .then((response) => {
          resolve(Vue._.get(response, "data"));
        })
        .catch((err) => reject(err));
    });
  },

  getSectors: function (userId) {
    return new Promise((resolve, reject) => {
      uaaAxios
        .get(`${process.env.VUE_APP_COMPANIES_API}/sectors?clientId=${userId}`)
        .then((response) => {
          resolve(Vue._.get(response, "data"));
        })
        .catch((err) => reject(err));
    });
  },

  getOdometer: function (specialistId) {
    return new Promise((resolve, reject) => {
      uaaAxios
        .get(
          `${process.env.VUE_APP_COMPANIES_API}/specialists/odometer/${specialistId}`
        )
        .then((response) => {
          resolve(Vue._.get(response, "data"));
        })
        .catch((err) => reject(err));
    });
  },

  getSizes: function (userId) {
    return new Promise((resolve, reject) => {
      uaaAxios
        .get(`${process.env.VUE_APP_COMPANIES_API}/sizes?clientId=${userId}`)
        .then((response) => {
          resolve(Vue._.get(response, "data"));
        })
        .catch((err) => reject(err));
    });
  },

  getRankingYears: function () {
    let userInfo = JSON.parse(localStorage.getItem("userInfo"));
    let clientId = "";
    if (userInfo && userInfo.principal && userInfo.principal.clientId) {
      clientId = userInfo.principal.clientId;
    }
    return new Promise((resolve, reject) => {
      if (clientId) {
        uaaAxios
          .get(
            `${process.env.VUE_APP_COMPANIES_API}/specialists/trajectory/${clientId}`
          )
          .then((response) => {
            resolve(Vue._.get(response, "data"));
          })
          .catch((err) => reject(err));
      } else {
        resolve();
      }
    });
  },

  getRankingSize: function () {
    let userInfo = JSON.parse(localStorage.getItem("userInfo"));
    let clientId = "";
    if (userInfo && userInfo.principal && userInfo.principal.clientId) {
      clientId = userInfo.principal.clientId;
    }
    return new Promise((resolve, reject) => {
      if (clientId) {
        uaaAxios
          .get(
            `${process.env.VUE_APP_COMPANIES_API}/specialists/size-index?specialistId=${clientId}`
          )
          .then((response) => {
            resolve(Vue._.get(response, "data"));
          })
          .catch((err) => reject(err));
      } else {
        resolve();
      }
    });
  },

  getCompanyName: function (tributaryId) {
    return new Promise((resolve, reject) => {
      uaaAxios
        .get(
          `${process.env.VUE_APP_COMPANIES_API}/companies/company-name?tributaryId=${tributaryId}`
        )
        .then((response) => {
          resolve(Vue._.get(response, "data"));
        })
        .catch((err) => reject(err));
    });
  },
};
