import uaaAxios from "@/utils/authAxios";
import axios from "axios";

export default {
  /* DT Survey */
  test: function (postId) {
    return new Promise((resolve, reject) => {
      let userInfo = JSON.parse(localStorage.getItem("userInfo"));
      let clientId = "";
      if (userInfo && userInfo.principal && userInfo.principal.clientId) {
        clientId = userInfo.principal.clientId;
        uaaAxios
          .post(
            `${process.env.VUE_APP_GET_ABSTRACT_TOKEN_API}/token?clientId=${clientId}`,
            {}
          )
          .then((token) => {
            let config = {
              headers: {
                Authorization: "Bearer " + token.data.accessToken,
              },
            };
            axios
              .get(
                `https://www.getabstract.com/api/library/v2/summaries/${postId}`,
                config
              )
              .then((response) => {
                resolve(response.data);
              })
              .catch((err) => reject(err));
          })
          .catch((err) => reject(err));
      } else {
        reject("no client Id");
      }
    });
  },
};
