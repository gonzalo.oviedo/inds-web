import uaaAxios from "@/utils/authAxios";
import Vue from "vue";

const getChartStagesAndActivities = [
  {
    // Actividad 1
    name: "Cambio Plan Prepago Mono Línea",
    data: [
      {
        // Etapa 1
        x: "Cambio Plan Móvil",
        y: [
          // Fechas Planificadas
          new Date("2022-02-01").getTime(),
          new Date("2022-03-11").getTime(),
        ],
        goals: [
          {
            // Fecha Actual
            name: "Break",
            value: new Date("2022-05-07").getTime(),
            strokeColor: "#CD2F2A",
          },
        ],
      },
      {
        x: "Cambio Plan Móvil",
        y: [
          // Fechas Reales
          new Date("2022-02-07").getTime(),
          new Date("2022-03-10").getTime(),
        ],
      },
    ],
  },
  {
    name: "Cambio Plan Prepago Multilínea",
    data: [
      {
        x: "Cambio Plan Móvil",
        y: [
          // Fechas Planificadas
          new Date("2022-03-02").getTime(),
          new Date("2022-03-11").getTime(),
        ],
        goals: [
          {
            name: "Break",
            // Fecha Actual
            value: new Date("2022-05-07").getTime(),
            strokeColor: "#CD2F2A",
          },
        ],
      },
      {
        x: "Cambio Plan Móvil",
        y: [
          // Fechas Reales
          new Date("2022-02-27").getTime(),
          new Date("2022-03-11").getTime(),
        ],
      },
    ],
  },
  {
    name: "Cambio Plan Postpago Sin Retención",
    data: [
      {
        x: "Cambio Plan Móvil",
        y: [
          // Fechas Planificadas
          new Date("2022-03-10").getTime(),
          new Date("2022-04-25").getTime(),
        ],
        goals: [
          {
            // Fecha Actual
            name: "Break",
            value: new Date("2022-05-07").getTime(),
            strokeColor: "#CD2F2A",
          },
        ],
      },
      {
        x: "Cambio Plan Móvil",
        y: [
          // Fechas Reales
          new Date("2022-03-10").getTime(),
          new Date("2022-05-05").getTime(),
        ],
      },
    ],
  },
  {
    // Actividad 4
    name: "Cambio Plan Postpago con Campañas",
    data: [
      {
        x: "Cambio Plan Móvil",
        y: [
          // Fechas Planificadas
          new Date("2022-03-15").getTime(),
          new Date("2022-05-05").getTime(),
        ],
        goals: [
          {
            // Fecha Actual
            name: "Break",
            value: new Date("2022-05-07").getTime(),
            strokeColor: "#CD2F2A",
          },
        ],
      },
      {
        x: "Cambio Plan Móvil",
        y: [
          // Fechas Reales
          new Date("2022-03-15").getTime(),
          new Date("2022-05-07").getTime(),
        ],
      },
    ],
  },
  {
    // Actividad 5
    name: "Cambio Plan Postpago con Devolución",
    data: [
      {
        // Etapa 1
        x: "Cambio Plan Móvil",
        y: [
          // Fechas Planificadas
          new Date("2022-05-01").getTime(),
          new Date("2022-07-10").getTime(),
        ],
        goals: [
          {
            // Fecha Actual
            name: "Break",
            value: new Date("2022-05-07").getTime(),
            strokeColor: "#CD2F2A",
          },
        ],
      },
    ],
  },
  {
    // Actividad 6
    name: "Cambio de Equipo en Venta MVP",
    data: [
      {
        // Etapa 2
        x: "Traslado Externo Domicilio",
        y: [
          // Fechas Planificadas
          new Date("2022-03-11").getTime(),
          new Date("2022-05-06").getTime(),
        ],
        goals: [
          {
            // Fecha Actual
            name: "Break",
            value: new Date("2022-05-07").getTime(),
            strokeColor: "#CD2F2A",
          },
        ],
      },
    ],
  },
  {
    // Actividad 7
    name: "Cambio de Equipo con Financiamiento",
    data: [
      {
        x: "Traslado Externo Domicilio",
        y: [new Date("2022-03-22").getTime(), new Date("2022-08-06").getTime()],
        goals: [
          {
            // Fecha Actual
            name: "Break",
            value: new Date("2022-05-07").getTime(),
            strokeColor: "#CD2F2A",
          },
        ],
      },
      {
        x: "Traslado Externo Domicilio",
        y: [
          // Fechas Reales
          new Date("2022-03-23").getTime(),
          new Date("2022-05-07").getTime(),
        ],
      },
    ],
  },
  {
    // Actividad 8
    name: "Cambio de Equipo más Seguro de Robo",
    data: [
      {
        // Etapa 2
        x: "Traslado Externo Domicilio",
        y: [
          // Fechas Planificadas
          new Date("2022-04-24").getTime(),
          new Date("2022-06-08").getTime(),
        ],
        goals: [
          {
            // Fecha Actual
            name: "Break",
            value: new Date("2022-05-07").getTime(),
            strokeColor: "#CD2F2A",
          },
        ],
      },
      {
        x: "Traslado Externo Domicilio",
        y: [
          // Fechas Reales
          new Date("2022-04-24").getTime(),
          new Date("2022-05-07").getTime(),
        ],
      },
    ],
  },
  {
    // Actividad 9
    name: "Cambio de Equipo Call Center",
    data: [
      {
        // Etapa 3
        x: "Traslado Externo Domicilio",
        y: [
          // Fechas Planificadas
          new Date("2022-06-02").getTime(),
          new Date("2022-08-15").getTime(),
        ],
        goals: [
          {
            // Fecha Actual
            name: "Break",
            value: new Date("2022-05-07").getTime(),
            strokeColor: "#CD2F2A",
          },
        ],
      },
    ],
  },
  {
    // Actividad 10
    name: "Cambio de Equipo en Venta MVP",
    data: [
      {
        // Etapa 3
        x: "Cesión de Línea",
        y: [
          // Fechas Planificadas
          new Date("2022-04-07").getTime(),
          new Date("2022-05-08").getTime(),
        ],
        goals: [
          {
            // Fecha Real
            name: "Break",
            value: new Date("2022-05-07").getTime(),
            strokeColor: "#CD2F2A",
          },
        ],
      },
      {
        x: "Cesión de Línea",
        y: [
          // Fechas Reales
          new Date("2022-04-09").getTime(),
          new Date("2022-05-06").getTime(),
        ],
      },
    ],
  },
  {
    name: "Cambio de Equipo en Venta CCT",
    data: [
      {
        x: "Cesión de Línea",
        y: [new Date("2022-05-05").getTime(), new Date("2022-06-23").getTime()],
        goals: [
          {
            name: "Break",
            value: new Date("2022-05-07").getTime(),
            strokeColor: "#CD2F2A",
          },
        ],
      },
      {
        x: "Cesión de Línea",
        y: [
          // Fechas Reales
          new Date("2022-05-05").getTime(),
          new Date("2022-05-07").getTime(),
        ],
      },
    ],
  },
  {
    name: "Cambio de sim TFI Sim en Mano",
    data: [
      {
        x: "Cesión de Línea",
        y: [new Date("2022-06-20").getTime(), new Date("2022-07-12").getTime()],
        goals: [
          {
            name: "Break",
            value: new Date("2022-05-07").getTime(),
            strokeColor: "#CD2F2A",
          },
        ],
      },
    ],
  },
  {
    name: "Cambio sin Equipo",
    data: [
      {
        x: "Cesión de Línea",
        y: [new Date("2022-07-04").getTime(), new Date("2022-08-28").getTime()],
        goals: [
          {
            name: "Break",
            value: new Date("2022-05-07").getTime(),
            strokeColor: "#CD2F2A",
          },
        ],
      },
    ],
  },
];

const getChartSummary = {
  data: {
    real: 14,
    esperado: 22,
    atraso: 8,
  },
  series: [
    {
      name: "Avance Real",
      data: [95, 0],
    },
    {
      name: "Desviación",
      data: [1, 0],
    },
    {
      name: "Avance Esperado",
      data: [0, 96],
    },
  ],
};
/*
const getAccomplishments2 = [
  {
    inicioPlanificado: "01-01-2022",
    fechaFinReal: "04-04-2022",
    nombreMiembro: "Responsable 1",
    desviacionAvance: "",
    avanceReal: "",
    desviacionInicio: "0",
    avancePlanificado: "",
    inicioReal: "01-01-2022",
    desviacionFin: "25",
    fechaFinPlanificado: "10-03-2022",
    nombre: "Levantamiento de Requerimiento",
    tipoFila: "etapa",
  },
  {
    inicioPlanificado: "01-01-2022",
    fechaFinReal: "22-02-2022",
    nombreMiembro: "Responsable 3",
    desviacionAvance: "0",
    avanceReal: "100",
    desviacionInicio: "0",
    avancePlanificado: "100",
    inicioReal: "01-01-2022",
    desviacionFin: "10",
    fechaFinPlanificado: "12-02-2022",
    nombre: "Requerimientos Contables y Documentos",
    tipoFila: "actividad",
  },
  {
    inicioPlanificado: "15-02-2022",
    fechaFinReal: "04-04-2022",
    nombreMiembro: "Responsable 3",
    desviacionAvance: "-93",
    avanceReal: "85",
    desviacionInicio: "3",
    avancePlanificado: "178",
    inicioReal: "18-01-2022",
    desviacionFin: "21",
    fechaFinPlanificado: "14-03-2022",
    nombre: "Requerimientos Financieros y Documentos",
    tipoFila: "actividad",
  },
  {
    inicioPlanificado: "04-02-2022",
    fechaFinReal: "01-04-2022",
    nombreMiembro: "Responsable 5",
    desviacionAvance: "-91",
    avanceReal: "60",
    desviacionInicio: "-1",
    avancePlanificado: "151",
    inicioReal: "03-02-2022",
    desviacionFin: "17",
    fechaFinPlanificado: "15-03-2022",
    nombre: "Perfiles de Usuario",
    tipoFila: "actividad",
  },
  {
    inicioPlanificado: "02-03-2022",
    fechaFinReal: "",
    nombreMiembro: "Responsable 8",
    desviacionAvance: "",
    avanceReal: "",
    desviacionInicio: "0",
    avancePlanificado: "",
    inicioReal: "02-03-2022",
    desviacionFin: "",
    fechaFinPlanificado: "05-05-2022",
    nombre: "Configuración Solución",
    tipoFila: "etapa",
  },
  {
    inicioPlanificado: "02-03-2022",
    fechaFinReal: "04-04-2022",
    nombreMiembro: "Responsable 13",
    desviacionAvance: "-92",
    avanceReal: "5",
    desviacionInicio: "0",
    avancePlanificado: "97",
    inicioReal: "02-03-2022",
    desviacionFin: "-1",
    fechaFinPlanificado: "05-04-2022",
    nombre: "Parametrizar Módulo Contable",
    tipoFila: "actividad",
  },
  {
    inicioPlanificado: "15-03-2022",
    fechaFinReal: "",
    nombreMiembro: "Responsable 13",
    desviacionAvance: "-65",
    avanceReal: "0",
    desviacionInicio: "2",
    avancePlanificado: "65",
    inicioReal: "17-03-2022",
    desviacionFin: "",
    fechaFinPlanificado: "15-04-2022",
    nombre: "Parametrizar Módulo Financiero",
    tipoFila: "actividad",
  },
  {
    inicioPlanificado: "03-04-2022",
    fechaFinReal: "",
    nombreMiembro: "Responsable 8",
    desviacionAvance: "",
    avanceReal: "0",
    desviacionInicio: "0",
    avancePlanificado: "6",
    inicioReal: "03-04-2022",
    fechaFinPlanificado: "20-04-2022",
    nombre: "Parametrizar Informes",
    tipoFila: "actividad",
  },
  {
    inicioPlanificado: "18-04-2022",
    fechaFinReal: "",
    nombreMiembro: "Responsable 5",
    desviacionAvance: "0",
    avanceReal: "0",
    desviacionInicio: "",
    avancePlanificado: "0",
    inicioReal: "",
    desviacionFin: "",
    fechaFinPlanificado: "05-05-2022",
    nombre: "Parametrizar Perfiles",
    tipoFila: "actividad",
  },
  {
    inicioPlanificado: "02-05-2022",
    fechaFinReal: "12-06-2022",
    nombreMiembro: "Responsable 3",
    desviacionAvance: "",
    avanceReal: "",
    desviacionInicio: "",
    avancePlanificado: "",
    inicioReal: "",
    desviacionFin: "",
    fechaFinPlanificado: "",
    nombre: "Pruebas de Usuario",
    tipoFila: "etapa",
  },
  {
    inicioPlanificado: "02-05-2022",
    fechaFinReal: "",
    nombreMiembro: "Responsable 1",
    desviacionAvance: "0",
    avanceReal: "0",
    desviacionInicio: "",
    avancePlanificado: "0",
    inicioReal: "",
    desviacionFin: "",
    fechaFinPlanificado: "14-05-2022",
    nombre: "Prueba de Vistas",
    tipoFila: "actividad",
  },
  {
    inicioPlanificado: "02-05-2022",
    fechaFinReal: "",
    nombreMiembro: "Responsable 1",
    desviacionAvance: "0",
    avanceReal: "0",
    desviacionInicio: "",
    avancePlanificado: "0",
    inicioReal: "",
    desviacionFin: "",
    fechaFinPlanificado: "24-05-2022",
    nombre: "Prueba de Flujos",
    tipoFila: "actividad",
  },
  {
    inicioPlanificado: "04-06-2022",
    fechaFinReal: "",
    nombreMiembro: "Responsable 13",
    desviacionAvance: "0",
    avanceReal: "0",
    desviacionInicio: "",
    avancePlanificado: "0",
    inicioReal: "",
    desviacionFin: "",
    fechaFinPlanificado: "12-06-2022",
    nombre: "Prueba de Informes",
    tipoFila: "actividad",
  },
];
*/

/*
const getAccomplishments = [
  {
    tipo: "Etapa",
    nombre: "Cambio Plan Móvil",
    avancePlanificado: 0.6666666667,
    avanceReal: 0.71,
    desviacionAvance: 0.04333333333,
    flagAvance: -1,
    finicioPlanificado: "1-2-2022",
    inicioReal: "7-2-2022",
    idesviacionInicio: 6,
    flagInicio: -1,
    fechaFinPlanificado: "10-7-2022",
    fechaFinReal: null,
    desviacionFin: null,
    flagTermino: 0,
    nombreMiembro: "Rodrigo",
    type: "0",
  },
  {
    tipo: "Actividad",
    nombre: "Cambio Plan Prepago Mono Línea",
    avancePlanificado: 1,
    avanceReal: 1,
    desviacionAvance: 0,
    flagAvance: 0,
    finicioPlanificado: "1-2-2022",
    inicioReal: "7-2-2022",
    idesviacionInicio: -6,
    flagInicio: -1,
    fechaFinPlanificado: "11-3-2022",
    fechaFinReal: "10-3-2022",
    desviacionFin: -1,
    flagTermino: -1,
    nombreMiembro: "Rodrigo",
    type: "1",
  },
  {
    tipo: "Actividad",
    nombre: "Cambio Plan Prepago Multilínea",
    avancePlanificado: 1,
    avanceReal: 1,
    desviacionAvance: 0,
    flagAvance: 0,
    finicioPlanificado: "2-3-2022",
    inicioReal: "27-2-2022",
    idesviacionInicio: 3,
    flagInicio: 1,
    fechaFinPlanificado: "11-3-2022",
    fechaFinReal: "11-3-2022",
    desviacionFin: 0,
    flagTermino: 0,
    nombreMiembro: "Rodrigo",
    type: "1",
  },
  {
    tipo: "Actividad",
    nombre: "Cambio Plan Postpago Sin Retención",
    avancePlanificado: 1,
    avanceReal: 0.4,
    desviacionAvance: -0.6,
    flagAvance: -1,
    finicioPlanificado: "10-3-2022",
    inicioReal: "10-3-2022",
    idesviacionInicio: 0,
    flagInicio: 0,
    fechaFinPlanificado: "25-4-2022",
    fechaFinReal: "5-5-2022",
    desviacionFin: null,
    flagTermino: 0,
    nombreMiembro: "Rodrigo",
    type: "1",
  },
  {
    tipo: "Actividad",
    nombre: "Cambio Plan Postpago con Campañas",
    avancePlanificado: 1,
    avanceReal: 0.12,
    desviacionAvance: -0.88,
    flagAvance: 1,
    finicioPlanificado: "15-3-2022",
    inicioReal: "15-3-2022",
    idesviacionInicio: 0,
    flagInicio: 0,
    fechaFinPlanificado: "5-5-2022",
    fechaFinReal: null,
    desviacionFin: null,
    flagTermino: 0,
    nombreMiembro: "Rodrigo",
    type: "1",
  },
  {
    tipo: "Actividad",
    nombre: "Cambio Plan Postpago con Devolución",
    avancePlanificado: 0.2428571429,
    avanceReal: 0.12,
    desviacionAvance: -0.1228571429,
    flagAvance: 1,
    finicioPlanificado: "1-5-2022",
    inicioReal: null,
    idesviacionInicio: 0,
    flagInicio: null,
    fechaFinPlanificado: "10-7-2022",
    fechaFinReal: null,
    desviacionFin: null,
    flagTermino: 0,
    nombreMiembro: "Rodrigo",
    type: "1",
  },
  {
    tipo: "Etapa",
    nombre: "Traslado Externo Domicilio",
    avancePlanificado: 0.4331210191,
    avanceReal: 0.28,
    desviacionAvance: -0.1531210191,
    flagAvance: -1,
    finicioPlanificado: "11-3-2022",
    inicioReal: "11-3-2022",
    idesviacionInicio: 0,
    flagInicio: 0,
    fechaFinPlanificado: "15-8-2022",
    fechaFinReal: null,
    desviacionFin: null,
    flagTermino: 0,
    nombreMiembro: "Ana",
    type: "0",
  },
  {
    tipo: "Actividad",
    nombre: "Cambio de Equipo en Venta MVP",
    avancePlanificado: 1,
    avanceReal: 0.42,
    desviacionAvance: -0.58,
    flagAvance: 1,
    finicioPlanificado: "11-3-2022",
    inicioReal: "11-3-2022",
    idesviacionInicio: 0,
    flagInicio: 0,
    fechaFinPlanificado: "6-5-2022",
    fechaFinReal: null,
    desviacionFin: null,
    flagTermino: null,
    nombreMiembro: "Ana",
    type: "1",
  },
  {
    tipo: "Actividad",
    nombre: "Cambio de Equipo con Financiamiento",
    avancePlanificado: 0.7307692308,
    avanceReal: 0.42,
    desviacionAvance: -0.3107692308,
    flagAvance: 1,
    finicioPlanificado: "22-3-2022",
    inicioReal: "23-3-2022",
    idesviacionInicio: 1,
    flagInicio: -1,
    fechaFinPlanificado: "8-6-2022",
    fechaFinReal: null,
    desviacionFin: null,
    flagTermino: null,
    nombreMiembro: "Ana",
    type: "1",
  },
  {
    tipo: "Actividad",
    nombre: "Cambio de Equipo más Seguro de Robo",
    avancePlanificado: 0.5333333333,
    avanceReal: 0,
    desviacionAvance: -0.5333333333,
    flagAvance: 0,
    finicioPlanificado: "24-4-2022",
    inicioReal: "24-4-2022",
    idesviacionInicio: null,
    flagInicio: 0,
    fechaFinPlanificado: "8-6-2022",
    fechaFinReal: null,
    desviacionFin: " ",
    flagTermino: null,
    nombreMiembro: "Ana",
    type: "1",
  },
  {
    tipo: "Actividad",
    nombre: "Cambio de Equipo Call Center",
    avancePlanificado: -0.2027027027,
    avanceReal: 0,
    desviacionAvance: 0.2027027027,
    flagAvance: 0,
    finicioPlanificado: "2-6-2022",
    inicioReal: null,
    idesviacionInicio: null,
    flagInicio: 0,
    fechaFinPlanificado: "15-8-2022",
    fechaFinReal: null,
    desviacionFin: null,
    flagTermino: 0,
    nombreMiembro: "Ana",
    type: "1",
  },
  {
    tipo: "Etapa",
    nombre: "Cesión de Línea",
    avancePlanificado: 0.2867132867,
    avanceReal: 0.28,
    desviacionAvance: -0.006713286713,
    flagAvance: -1,
    finicioPlanificado: "7-4-2022",
    inicioReal: "9-4-2022",
    idesviacionInicio: 2,
    flagInicio: 0,
    fechaFinPlanificado: "28-8-2022",
    fechaFinReal: null,
    desviacionFin: null,
    flagTermino: 0,
    nombreMiembro: "Rodolfo",
    type: "0",
  },
  {
    tipo: "Actividad",
    nombre: "Cambio de Equipo en Venta MVP",
    avancePlanificado: 1,
    avanceReal: 0.9,
    desviacionAvance: -0.1,
    flagAvance: -1,
    finicioPlanificado: "7-4-2022",
    inicioReal: "9-4-2022",
    idesviacionInicio: 0,
    flagInicio: 0,
    fechaFinPlanificado: "8-5-2022",
    fechaFinReal: "6-5-2022",
    desviacionFin: 10,
    flagTermino: -1,
    nombreMiembro: "Rodolfo",
    type: "1",
  },
  {
    tipo: "Actividad",
    nombre: "Cambio de Equipo en Venta CCT",
    avancePlanificado: 1,
    avanceReal: 0.42,
    desviacionAvance: -0.58,
    flagAvance: -1,
    finicioPlanificado: "5-5-2022",
    inicioReal: "5-5-2022",
    idesviacionInicio: 0,
    flagInicio: 0,
    fechaFinPlanificado: "23-6-2022",
    fechaFinReal: null,
    desviacionFin: null,
    flagTermino: 0,
    nombreMiembro: "Rodolfo",
    type: "1",
  },
  {
    tipo: "Actividad",
    nombre: " Cambio de sim TFI Sim en Mano",
    avancePlanificado: -1.5,
    avanceReal: 0,
    desviacionAvance: 1.5,
    flagAvance: 0,
    finicioPlanificado: "20-6-2022",
    inicioReal: null,
    idesviacionInicio: null,
    flagInicio: 0,
    fechaFinPlanificado: "12-7-2022",
    fechaFinReal: null,
    desviacionFin: null,
    flagTermino: 0,
    nombreMiembro: "Rodolfo",
    type: "1",
  },
  {
    tipo: "Actividad",
    nombre: "Cambio sin Equipo",
    avancePlanificado: -0.8545454545,
    avanceReal: 0,
    desviacionAvance: 0.8545454545,
    flagAvance: 0,
    finicioPlanificado: "4-7-2022",
    inicioReal: null,
    idesviacionInicio: null,
    flagInicio: 0,
    fechaFinPlanificado: "28-8-2022",
    fechaFinReal: null,
    desviacionFin: null,
    flagTermino: 0,
    nombreMiembro: "Rodolfo",
    type: "1",
  },
];
*/
const getActivities = [
  {
    id: 1,
    orden: 1,
    nombre: "API Planificación",
    inicio: "2019-03-05",
    termino: "2019-03-15",
    etapaId: 1,
    proyectoId: 1,
    responsableId: 1,
  },
];

export default {
  setup: function () {
    return new Promise((resolve, reject) => {
      uaaAxios
        .post(`${process.env.VUE_APP_COMPANIES_API}/plan/setup/1`)
        .then((response) => {
          let data = response.data;
          console.log(data);
          resolve(data);
        })
        .catch((err) => reject(err));
    });
  },
  saveStage(requestStage) {
    console.log(
      "call For planificationService.js.saveStage -- called with requestStage: "
    );
    console.log(requestStage);

    /*requestTender = this.prepareRequestTender(requestTender);
    console.log(
      "callForTenderService.js.saveRequestTender - repared requestTender:"
    );
    console.log(requestTender);
    */

    return new Promise((resolve, reject) => {
      uaaAxios
        .post(
          `${process.env.VUE_APP_COMPANIES_API}/plan/saveStage`,
          requestStage
        )
        .then((response) => {
          resolve(Vue._.get(response, "data"));
        })
        .catch((err) => reject(err));
    });
  },
  saveActivity(requestActivity) {
    console.log(
      "call For planificationService.js.saveActivity -- called with requestActivity: "
    );
    console.log(requestActivity);

    /*requestTender = this.prepareRequestTender(requestTender);
    console.log(
      "callForTenderService.js.saveRequestTender - repared requestTender:"
    );
    console.log(requestTender);
    */

    return new Promise((resolve, reject) => {
      uaaAxios
        .post(
          `${process.env.VUE_APP_COMPANIES_API}/plan/saveActivity`,
          requestActivity
        )
        .then((response) => {
          resolve(Vue._.get(response, "data"));
        })
        .catch((err) => reject(err));
    });
  },
  savePlanificationTracing(request) {
    console.log("call For planificationService.js.savePlanificationTracing");
    console.log(request);

    return new Promise((resolve, reject) => {
      uaaAxios
        .post(
          `${process.env.VUE_APP_COMPANIES_API}/plan/savePlanificationTracing`,
          request
        )
        .then((response) => {
          resolve(Vue._.get(response, "data"));
        })
        .catch((err) => reject(err));
    });
  },
  deleteStage: function (id) {
    return new Promise((resolve, reject) => {
      uaaAxios
        .delete(`${process.env.VUE_APP_COMPANIES_API}/plan/deleteStage/${id}`)
        .then((response) => {
          let data = response.data;
          resolve(data);
        })
        .catch((err) => reject(err));
    });
  },
  deleteActivity: function (id) {
    return new Promise((resolve, reject) => {
      uaaAxios
        .delete(
          `${process.env.VUE_APP_COMPANIES_API}/plan/deleteActivity/${id}`
        )
        .then((response) => {
          let data = response.data;
          resolve(data);
        })
        .catch((err) => reject(err));
    });
  },
  stages: function () {
    return new Promise((resolve, reject) => {
      uaaAxios
        .get(`${process.env.VUE_APP_COMPANIES_API}/budget/stages`)
        .then((response) => {
          let data = response.data;
          resolve(data);
        })
        .catch((err) => reject(err));
    });
  },
  getProjectStages: function (projectId) {
    return new Promise((resolve, reject) => {
      uaaAxios
        .get(
          `${process.env.VUE_APP_COMPANIES_API}/plan/getProjectStages/${projectId}`
        )
        .then((response) => {
          let data = response.data;
          resolve(data);
        })
        .catch((err) => reject(err));
    });
  },
  accomplishments: function (projectId) {
    return new Promise((resolve, reject) => {
      uaaAxios
        .get(
          `${process.env.VUE_APP_COMPANIES_API}/plan/getAccomplishmentTable/${projectId}`
        )
        .then((response) => {
          let data = response.data;
          resolve(data);
          console.log(data);
        })
        .catch((err) => reject(err));
    });
  },
  getActivitiesByStage: function (stageId) {
    return new Promise((resolve, reject) => {
      uaaAxios
        .post(
          `${process.env.VUE_APP_COMPANIES_API}/plan/getActivitiesByStage/${stageId}`
        )
        .then((response) => {
          let data = response.data;
          console.log(data);
          //resolve(data);
          resolve(getActivities);
        })
        .catch((err) => reject(err));
    });
  },
  getResponsible: function () {
    return new Promise((resolve, reject) => {
      uaaAxios
        .get(`${process.env.VUE_APP_COMPANIES_API}/plan/getResponsible`)
        .then((response) => {
          let data = response.data;
          console.log(data);
          resolve(data);
        })
        .catch((err) => reject(err));
    });
  },
  getChartSummary: function () {
    return new Promise((resolve, reject) => {
      uaaAxios
        .get(`${process.env.VUE_APP_COMPANIES_API}/plan/getChartSummary`)
        .then((response) => {
          let data = response.data;
          console.log(data);
          //resolve(data);
          resolve(getChartSummary);
        })
        .catch((err) => reject(err));
    });
  },
  getActivities: function () {
    return new Promise((resolve, reject) => {
      uaaAxios
        .get(`${process.env.VUE_APP_COMPANIES_API}/plan/getActivities`)
        .then((response) => {
          let data = response.data;
          console.log(data);
          resolve(data);
        })
        .catch((err) => reject(err));
    });
  },
  getChartStagesAndActivities: function () {
    return new Promise((resolve) => {
      resolve(getChartStagesAndActivities);
      /*
      uaaAxios
        .get(
          `${process.env.VUE_APP_COMPANIES_API}/plan/getChartStagesAndActivities`
        )
        .then((response) => {
          let data = response.data;
          console.log(data);
          // resolve(data);
          resolve(getChartStagesAndActivities);
        })
        .catch((err) => reject(err));
      */
    });
  },
  getPercentages: function () {
    return new Promise((resolve, reject) => {
      uaaAxios
        .get(`${process.env.VUE_APP_COMPANIES_API}/plan/getPercentages`)
        .then((response) => {
          let data = response.data;
          resolve(data);
        })
        .catch((err) => reject(err));
    });
  },
};
