import uaaAxios from "@/utils/authAxios";

const apiUrl = process.env.VUE_APP_COMPANIES_API;

export default {
  getSummaryKPINegocio: function () {
    return new Promise((resolve, reject) => {
      uaaAxios
        .get(`${apiUrl}/kpis/chartSumaryKPINegocio`)
        .then((response) => {
          resolve(response.data);
          //console.log(response.data);
          //resolve(responseSumaryKPINegocios);
        })
        .catch((err) => reject(err));
    });
  },
  getHistoryKPINegocio: function () {
    return new Promise((resolve, reject) => {
      uaaAxios
        .get(`${apiUrl}/kpis/chartHistoryKPINegocio`)
        .then((response) => {
          resolve(response.data);
          console.log(response.data);
          //resolve(responseHistoryKPINegocios);
        })
        .catch((err) => reject(err));
    });
  },
  getRatioHistoryKPINegocio: function () {
    return new Promise((resolve, reject) => {
      uaaAxios
        .get(`${apiUrl}/kpis/chartRatioKPINegocio`)
        .then((response) => {
          resolve(response.data);
          console.log(response.data);
          //resolve(responseRatioKPINegocios);
        })
        .catch((err) => reject(err));
    });
  },
  getCompositionKPINegocio: function () {
    return new Promise((resolve, reject) => {
      uaaAxios
        .get(`${apiUrl}/kpis/chartCompositionKPINegocio`)
        .then((response) => {
          console.log(response.data);
          resolve(response.data);
          //resolve(responseCompositionKPINegocios);
        })
        .catch((err) => reject(err));
    });
  },

  getCostEffectiveness: function () {
    return new Promise((resolve, reject) => {
      uaaAxios
        .get(`${apiUrl}/kpis/chartCostEffectiveness`)
        .then((response) => {
          //console.log(response.data);
          resolve(response.data);
          //resolve(costEffectiveness);
        })
        .catch((err) => reject(err));
    });
  },
  getCostReduction: function () {
    return new Promise((resolve, reject) => {
      uaaAxios
        .get(`${apiUrl}/kpis/chartCostReduction`)
        .then((response) => {
          console.log(response.data);
          resolve(response.data);
          //resolve(costReduction);
        })
        .catch((err) => reject(err));
    });
  },
  getExternalCustomer: function () {
    return new Promise((resolve, reject) => {
      uaaAxios
        .get(`${apiUrl}/kpis/chartExternalCustomer`)
        .then((response) => {
          console.log(response.data);
          resolve(response.data);
          //resolve(externalCustomer);
        })
        .catch((err) => reject(err));
    });
  },
  getInternalCustomer: function () {
    return new Promise((resolve, reject) => {
      uaaAxios
        .get(`${apiUrl}/kpis/chartInternalCustomer`)
        .then((response) => {
          console.log(response.data);
          resolve(response.data);
          //resolve(internalCustomer);
        })
        .catch((err) => reject(err));
    });
  },
  getTakeDecisions: function () {
    return new Promise((resolve, reject) => {
      uaaAxios
        .get(`${apiUrl}/kpis/chartTakeDecisions`)
        .then((response) => {
          console.log(response.data);
          resolve(response.data);
          //resolve(takeDecisions);
        })
        .catch((err) => reject(err));
    });
  },
  getFirstLine: function () {
    return new Promise((resolve, reject) => {
      uaaAxios
        .get(`${apiUrl}/kpis/chartFirstLine`)
        .then((response) => {
          console.log(response.data);
          resolve(response.data);
          //resolve(firstLine);
        })
        .catch((err) => reject(err));
    });
  },
};
