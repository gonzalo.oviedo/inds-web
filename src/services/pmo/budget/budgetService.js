import uaaAxios from "@/utils/authAxios";

export default {
  getAll: function () {
    return new Promise((resolve, reject) => {
      uaaAxios
        .get(`${process.env.VUE_APP_COMPANIES_API}/budget/all`)
        .then((response) => {
          let data = response.data;
          resolve(data);
        })
        .catch((err) => reject(err));
    });
  },
  getInitialAndBalance: function () {
    return new Promise((resolve, reject) => {
      uaaAxios
        .get(`${process.env.VUE_APP_COMPANIES_API}/budget/getInitialAndBalance`)
        .then((response) => {
          let data = response.data;
          resolve(data);
        })
        .catch((err) => reject(err));
    });
  },
  getTypes: function () {
    return new Promise((resolve, reject) => {
      uaaAxios
        .get(`${process.env.VUE_APP_COMPANIES_API}/budget/getTypes`)
        .then((response) => {
          let data = response.data;
          resolve(data);
        })
        .catch((err) => reject(err));
    });
  },
  getConcepts: function () {
    return new Promise((resolve, reject) => {
      uaaAxios
        .get(`${process.env.VUE_APP_COMPANIES_API}/budget/getConcepts`)
        .then((response) => {
          let data = response.data;
          resolve(data);
        })
        .catch((err) => reject(err));
    });
  },
  saveExpense: function (date, type, concept, expense) {
    return new Promise((resolve, reject) => {
      uaaAxios
        .post(`${process.env.VUE_APP_COMPANIES_API}/budget/saveExpense`, {
          date: date,
          type: type,
          concept: concept,
          expense: expense,
        })
        .then((response) => {
          resolve(response);
        })
        .catch((err) => reject(err));
    });
  },
  getPieTwo: function () {
    return new Promise((resolve, reject) => {
      uaaAxios
        .get(`${process.env.VUE_APP_COMPANIES_API}/budget/getPieTwo`)
        .then((response) => {
          let data = response.data;
          resolve(data);
        })
        .catch((err) => reject(err));
    });
  },
  timeLineChart: function () {
    return new Promise((resolve, reject) => {
      uaaAxios
        .get(`${process.env.VUE_APP_COMPANIES_API}/budget/timeLineChart`)
        .then((response) => {
          let data = response.data;
          resolve(data);
        })
        .catch((err) => reject(err));
    });
  },
  stages: function () {
    return new Promise((resolve, reject) => {
      uaaAxios
        .get(`${process.env.VUE_APP_COMPANIES_API}/budget/stages`)
        .then((response) => {
          let data = response.data;
          resolve(data);
        })
        .catch((err) => reject(err));
    });
  },
  stagesInExistence: function () {
    return new Promise((resolve, reject) => {
      uaaAxios
        .get(`${process.env.VUE_APP_COMPANIES_API}/budget/stagesInExistence`)
        .then((response) => {
          let data = response.data;
          resolve(data);
        })
        .catch((err) => reject(err));
    });
  },
};
