import uaaAxios from "@/utils/authAxios";

const apiURL = process.env.VUE_APP_COMPANIES_API;
export default {
  getSummaryObjectives: function () {
    return new Promise((resolve, reject) => {
      uaaAxios
        .get(`${apiURL}/objectives/chartSummaryObjectives`)
        .then((response) => {
          resolve(response.data);
          //console.log(response);
          //resolve(getSummaryObjectives);
        })
        .catch((err) => reject(err));
    });
  },
  chartCircleData: function () {
    return new Promise((resolve, reject) => {
      uaaAxios
        .get(`${apiURL}/objectives/chartCircleData`)
        .then((response) => {
          resolve(response.data);
          console.log("getCircleData", response);
          //resolve(getCircleData);
        })
        .catch((err) => reject(err));
    });
  },
};
