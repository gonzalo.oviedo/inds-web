//import uaaAxios from "@/utils/authAxios";
import uaaAxios from "axios";
const apiUrl = process.env.VUE_APP_COMPANIES_API;

const chartSummaryEngagement = {
  series: [
    {
      name: "% Retensión",
      data: [32, 38, 43, 46, 30, 35, 23, 31, 34, 32, 41, 45],
    },
  ],
};
const chartRetentionRate = {
  series: [
    {
      name: "% Retensión",
      data: [32, 38, 43, 46, 30, 35, 23, 31, 34, 32, 41, 45],
    },
  ],
};
const chartConversionRate = {
  series: [
    {
      name: "Conversión",
      data: [27, 28, 26, 32, 31, 43, 46, 46, 38, 42, 48, 50],
    },
  ],
};
const chartWinBackRate = {
  series: [
    {
      name: "% Predictibilidad",
      data: [17, 28, 36, 55, 81, 69, 76, 63, 72, 76, 81, 79],
    },
  ],
};
const chartDigitalClient = {
  series: [
    {
      name: "Julio 2021",
      data: [
        { x: "0", y: 615 },
        { x: "1", y: 698 },
        { x: "2", y: 882 },
        { x: "3", y: 1182 },
        { x: "4", y: 1228 },
        { x: "5", y: 1347 },
        { x: "6", y: 1486 },
        { x: "7", y: 1742 },
        { x: "8", y: 1154 },
        { x: "9", y: 1154 },
        { x: "10", y: 1154 },
        { x: "11", y: 1254 },
        { x: "12", y: 1250 },
      ].reverse(),
    },
    {
      name: "Agosto 2021",
      data: [
        { x: "0", y: 1054 },
        { x: "1", y: 1054 },
        { x: "2", y: 1054 },
        { x: "3", y: 1054 },
        { x: "4", y: 1054 },
        { x: "5", y: 1054 },
        { x: "6", y: 1054 },
        { x: "7", y: 1054 },
        { x: "8", y: 1054 },
        { x: "9", y: 1054 },
        { x: "10", y: 1054 },
        { x: "11", y: 50 },
        { x: "12", y: 0 },
      ],
    },
    {
      name: "Septiembre 2021",
      data: [
        { x: "0", y: 1054 },
        { x: "1", y: 1054 },
        { x: "2", y: 1054 },
        { x: "3", y: 1054 },
        { x: "4", y: 1054 },
        { x: "5", y: 1054 },
        { x: "6", y: 1054 },
        { x: "7", y: 1054 },
        { x: "8", y: 1054 },
        { x: "9", y: 1054 },
        { x: "10", y: 50 },
        { x: "11", y: 0 },
        { x: "12", y: 0 },
      ],
    },
    {
      name: "Octubre 2021",
      data: [
        { x: "0", y: 1054 },
        { x: "1", y: 1054 },
        { x: "2", y: 1054 },
        { x: "3", y: 1054 },
        { x: "4", y: 1054 },
        { x: "5", y: 1054 },
        { x: "6", y: 1054 },
        { x: "7", y: 1054 },
        { x: "8", y: 1054 },
        { x: "9", y: 50 },
        { x: "10", y: 0 },
        { x: "11", y: 0 },
        { x: "12", y: 0 },
      ],
    },
    {
      name: "Noviembre 2021",
      data: [
        { x: "0", y: 1054 },
        { x: "1", y: 1054 },
        { x: "2", y: 1054 },
        { x: "3", y: 1054 },
        { x: "4", y: 1054 },
        { x: "5", y: 1054 },
        { x: "6", y: 1054 },
        { x: "7", y: 1054 },
        { x: "8", y: 50 },
        { x: "9", y: 0 },
        { x: "10", y: 0 },
        { x: "11", y: 0 },
        { x: "12", y: 0 },
      ],
    },
    {
      name: "Diciembre 2021",
      data: [
        { x: "0", y: 1054 },
        { x: "1", y: 1054 },
        { x: "2", y: 1054 },
        { x: "3", y: 1054 },
        { x: "4", y: 1054 },
        { x: "5", y: 1054 },
        { x: "6", y: 1054 },
        { x: "7", y: 50 },
        { x: "8", y: 0 },
        { x: "9", y: 0 },
        { x: "10", y: 0 },
        { x: "11", y: 0 },
        { x: "12", y: 0 },
      ],
    },
    {
      name: "Enero 2022",
      data: [
        { x: "0", y: 1054 },
        { x: "1", y: 1054 },
        { x: "2", y: 1054 },
        { x: "3", y: 1054 },
        { x: "4", y: 1054 },
        { x: "5", y: 1054 },
        { x: "6", y: 50 },
        { x: "7", y: 0 },
        { x: "8", y: 0 },
        { x: "9", y: 0 },
        { x: "10", y: 0 },
        { x: "11", y: 0 },
        { x: "12", y: 0 },
      ],
    },
    {
      name: "Febrero 2022",
      data: [
        { x: "0", y: 1054 },
        { x: "1", y: 1054 },
        { x: "2", y: 1054 },
        { x: "3", y: 1054 },
        { x: "4", y: 1054 },
        { x: "5", y: 50 },
        { x: "6", y: 0 },
        { x: "7", y: 0 },
        { x: "8", y: 0 },
        { x: "9", y: 0 },
        { x: "10", y: 0 },
        { x: "11", y: 0 },
        { x: "12", y: 0 },
      ],
    },
    {
      name: "Marzo 2022",
      data: [
        { x: "0", y: 1054 },
        { x: "1", y: 1054 },
        { x: "2", y: 1054 },
        { x: "3", y: 1054 },
        { x: "4", y: 50 },
        { x: "5", y: 0 },
        { x: "6", y: 0 },
        { x: "7", y: 0 },
        { x: "8", y: 0 },
        { x: "9", y: 0 },
        { x: "10", y: 0 },
        { x: "11", y: 0 },
        { x: "12", y: 0 },
      ],
    },
    {
      name: "Abril 2022",
      data: [
        { x: "0", y: 1054 },
        { x: "1", y: 1054 },
        { x: "2", y: 1054 },
        { x: "3", y: 50 },
        { x: "4", y: 0 },
        { x: "5", y: 0 },
        { x: "6", y: 0 },
        { x: "7", y: 0 },
        { x: "8", y: 0 },
        { x: "9", y: 0 },
        { x: "10", y: 0 },
        { x: "11", y: 0 },
        { x: "12", y: 0 },
      ],
    },
    {
      name: "Mayo 2022",
      data: [
        { x: "0", y: 1054 },
        { x: "1", y: 1054 },
        { x: "2", y: 50 },
        { x: "3", y: 0 },
        { x: "4", y: 0 },
        { x: "5", y: 0 },
        { x: "6", y: 0 },
        { x: "7", y: 0 },
        { x: "8", y: 0 },
        { x: "9", y: 0 },
        { x: "10", y: 0 },
        { x: "11", y: 0 },
        { x: "12", y: 0 },
      ],
    },
    {
      name: "Junio 2022",
      data: [
        { x: "0", y: 1054 },
        { x: "1", y: 50 },
        { x: "2", y: 0 },
        { x: "3", y: 0 },
        { x: "4", y: 0 },
        { x: "5", y: 0 },
        { x: "6", y: 0 },
        { x: "7", y: 0 },
        { x: "8", y: 0 },
        { x: "9", y: 0 },
        { x: "10", y: 0 },
        { x: "11", y: 0 },
        { x: "12", y: 0 },
      ],
    },
    {
      name: "Julio 2022",
      data: [
        { x: "0", y: 1054 },
        { x: "1", y: 0 },
        { x: "2", y: 0 },
        { x: "3", y: 0 },
        { x: "4", y: 0 },
        { x: "5", y: 0 },
        { x: "6", y: 0 },
        { x: "7", y: 0 },
        { x: "8", y: 0 },
        { x: "9", y: 0 },
        { x: "10", y: 0 },
        { x: "11", y: 0 },
        { x: "12", y: 0 },
      ],
    },
  ].reverse(),
};
const chartAgileMethodologies = {
  series: [
    {
      name: "LTV",
      data: [43, 54, 62, 72, 88, 100, 31, 39, 43, 48, 61, 66, 69],
    },
  ],
};
const chartAdherenceMethodologies = {
  series: [75],
};
const chartTechnologyAdoption = {
  series: [83],
};
const chartCircleConversionRate = {
  series: [55],
};

export default {
  getSummaryEngagement: function () {
    return new Promise((resolve, reject) => {
      uaaAxios
        // .get(`${apiUrl}/agility/chartSummaryEngagement`)
        .get(`${apiUrl}/agility/chartFlowEfficiency`)
        .then((response) => {
          console.log(response.data);
          // resolve(response.data);
          resolve(chartSummaryEngagement);
        })
        .catch((err) => reject(err));
    });
  },
  getRetentionRate: function () {
    return new Promise((resolve, reject) => {
      uaaAxios
        // .get(`${apiUrl}/agility/chartRetentionRate`)
        .get(`${apiUrl}/agility/chartFlowEfficiency`)
        .then((response) => {
          console.log(response.data);
          // resolve(response.data);
          resolve(chartRetentionRate);
        })
        .catch((err) => {
          console.log(err);
          reject(chartRetentionRate);
        });
    });
  },
  getConversionRate: function () {
    return new Promise((resolve, reject) => {
      uaaAxios
        // .get(`${apiUrl}/agility/chartConversionRate`)
        .get(`${apiUrl}/agility/chartFlowEfficiency`)
        .then((response) => {
          console.log(response.data);
          // resolve(response.data);
          resolve(chartConversionRate);
        })
        .catch((err) => reject(err));
    });
  },
  getWinBackRate: function () {
    return new Promise((resolve, reject) => {
      uaaAxios
        // .get(`${apiUrl}/agility/chartWinBackRate`)
        .get(`${apiUrl}/agility/chartFlowEfficiency`)
        .then((response) => {
          console.log(response.data);
          // resolve(response.data);
          resolve(chartWinBackRate);
        })
        .catch((err) => reject(err));
    });
  },
  getDigitalClient: function () {
    return new Promise((resolve, reject) => {
      uaaAxios
        // .get(`${apiUrl}/agility/chartDigitalClient`)
        .get(`${apiUrl}/agility/chartFlowEfficiency`)
        .then((response) => {
          console.log(response.data);
          // resolve(response.data);
          resolve(chartDigitalClient);
        })
        .catch((err) => reject(err));
    });
  },
  getAgileMethodologies: function () {
    return new Promise((resolve, reject) => {
      uaaAxios
        // .get(`${apiUrl}/agility/chartAgileMethodologies`)
        .get(`${apiUrl}/agility/chartFlowEfficiency`)
        .then((response) => {
          console.log(response.data);
          // resolve(response.data);
          resolve(chartAgileMethodologies);
        })
        .catch((err) => reject(err));
    });
  },
  getAdherenceMethodologies: function () {
    return new Promise((resolve, reject) => {
      uaaAxios
        // .get(`${apiUrl}/agility/chartAdherenceMethodologies`)
        .get(`${apiUrl}/agility/chartFlowEfficiency`)
        .then((response) => {
          console.log(response.data);
          // resolve(response.data);
          resolve(chartAdherenceMethodologies);
        })
        .catch((err) => reject(err));
    });
  },
  getTechnologyAdoption: function () {
    return new Promise((resolve, reject) => {
      uaaAxios
        // .get(`${apiUrl}/agility/chartTechnologyAdoption`)
        .get(`${apiUrl}/agility/chartFlowEfficiency`)
        .then((response) => {
          console.log(response.data);
          // resolve(response.data);
          resolve(chartTechnologyAdoption);
        })
        .catch((err) => reject(err));
    });
  },
  getCircleConversionRate: function () {
    return new Promise((resolve, reject) => {
      uaaAxios
        // .get(`${apiUrl}/agility/chartCircleConversionRate`)
        .get(`${apiUrl}/agility/chartFlowEfficiency`)
        .then((response) => {
          console.log(response.data);
          // resolve(response.data);
          resolve(chartCircleConversionRate);
        })
        .catch((err) => reject(err));
    });
  },
};
