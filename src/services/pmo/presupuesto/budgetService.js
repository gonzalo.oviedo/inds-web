import uaaAxios from "@/utils/authAxios";

const apiURL = process.env.VUE_APP_COMPANIES_API;
/*
const chartSummaryBudget = {
  resume: [
    {
      description: "Gasto Real",
      value: "MM$ 2.191",
      percentage: "(70%)",
    },
    {
      description: "Disponible",
      value: "MM$ 934",
      percentage: "(30%)",
    },
    {
      description: "Gasto Planificado",
      value: "MM$ 2.000",
      percentage: "(64%)",
    },
    {
      description: "Presupuesto Total:",
      value: "MM$ 3.125",
      percentage: "",
    },
  ],
  series: [
    {
      name: "Gasto Real",
      data: [2191, 0, 0],
    },
    {
      name: "Saldo",
      data: [934, 0, 0],
    },
    {
      name: "Gasto Planificado a hoy",
      data: [0, 2000, 0],
    },
    {
      name: "Desviación presupuesto",
      data: [0, 191, 0],
    },
    {
      name: "Presupuesto Total",
      data: [0, 0, 3125],
    },
  ],
};

const chartPeriodTrack = [
  {
    name: "Presupuesto",
    type: "column",
    data: [211, 203, 331, 340, 441, 449, 565, 585],
  },
  {
    name: "Gasto",
    type: "column",
    data: [100, 190, 207, 260, 304, 350, 400, 380],
  },
  {
    name: "Gasto/Presupuesto %",
    type: "line",
    data: [47, 94, 63, 69, 78, 80, 71, 65],
  },
];
const chartTrackByCostCenter = [
  {
    name: "Gasto",
    data: [660, 440, 810, 125, 540, 120],
  },
  {
    name: "Presupuesto",
    data: [610, 540, 660, 520, 270, 140],
  },
];

const chartCostCenter = [
  {
    name: "Recursos Humanos",
    data: [440, 550, 410, 370, 220, 403, 201],
  },
  {
    name: "Tecnología",
    data: [503, 320, 330, 50, 130, 430, 320],
  },
  {
    name: "Administración y Finanzas",
    data: [120, 170, 110, 90, 150, 110, 200],
  },
  {
    name: "Operaciones",
    data: [90, 70, 50, 80, 60, 90, 40],
  },
  {
    name: "Comercial",
    data: [250, 120, 190, 320, 250, 240, 100],
  },
];
*/

export default {
  getSummaryBudget: function () {
    return new Promise((resolve, reject) => {
      uaaAxios
        .get(`${apiURL}/budget/chartSummaryBudget`)
        .then((response) => {
          resolve(response.data);
          console.log(response);
          //resolve(chartSummaryBudget);
        })
        .catch((err) => reject(err));
    });
  },
  getPeriodTrack: function () {
    return new Promise((resolve, reject) => {
      uaaAxios
        .get(`${apiURL}/budget/chartPeriodTrack`)
        .then((response) => {
          // console.log(response);
          resolve(response.data);
          //resolve(chartPeriodTrack);
        })
        .catch((err) => reject(err));
    });
  },
  getTrackByCostCenter: function () {
    return new Promise((resolve, reject) => {
      uaaAxios
        .get(`${apiURL}/budget/chartTrackByCostCenter`)
        .then((response) => {
          console.log(response);
          resolve(response.data);
          //resolve(chartTrackByCostCenter);
        })
        .catch((err) => reject(err));
    });
  },
  getCostCenter: function () {
    return new Promise((resolve, reject) => {
      uaaAxios
        .get(`${apiURL}/budget/chartCostCenter`)
        .then((response) => {
          console.log(response);
          resolve(response.data);
          // resolve(chartCostCenter);
        })
        .catch((err) => reject(err));
    });
  },
};
