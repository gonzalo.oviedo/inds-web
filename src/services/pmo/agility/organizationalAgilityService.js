import uaaAxios from "@/utils/authAxios";

const apiUrl = process.env.VUE_APP_COMPANIES_API;

export default {
  getFlowEfficiency: function () {
    return new Promise((resolve, reject) => {
      uaaAxios
        .get(`${apiUrl}/agility/chartFlowEfficiency`)
        .then((response) => {
          resolve(response.data);
          //console.log(response.data);
          // resolve(flowEfficiency);
        })
        .catch((err) => reject(err));
    });
  },
  getFlowRate: function () {
    return new Promise((resolve, reject) => {
      uaaAxios
        .get(`${apiUrl}/agility/chartFlowRate`)
        .then((response) => {
          resolve(response.data);
          //console.log(response.data);
          //resolve(flowRate);
        })
        .catch((err) => reject(err));
    });
  },
  getFlowPredictability: function () {
    return new Promise((resolve, reject) => {
      uaaAxios
        .get(`${apiUrl}/agility/chartFlowPredictability`)
        .then((response) => {
          resolve(response.data);
          console.log(response.data);
          //resolve(flowPredictability);
        })
        .catch((err) => reject(err));
    });
  },
  getFlowTime: function () {
    return new Promise((resolve, reject) => {
      uaaAxios
        .get(`${apiUrl}/agility/chartFlowTime`)
        .then((response) => {
          console.log(response.data);
          resolve(response.data);
          //resolve(flowTime);
        })
        .catch((err) => reject(err));
    });
  },
};
