import uaaAxios from "@/utils/authAxios";

const apiURL = process.env.VUE_APP_COMPANIES_API;

export default {
  getSummaryRisk: function () {
    return new Promise((resolve, reject) => {
      uaaAxios
        .get(`${apiURL}/risk/chartSummaryRisk`)
        .then((response) => {
          resolve(response.data);
          console.log(response);
          //resolve(getSummaryRisk);
        })
        .catch((err) => reject(err));
    });
  },
  getRiskMatriz: function () {
    return new Promise((resolve, reject) => {
      uaaAxios
        .get(`${apiURL}/risk/chartRiskMatriz`)
        .then((response) => {
          resolve(response.data);
          console.log(response);
          //resolve(getRiskMatriz);
        })
        .catch((err) => reject(err));
    });
  },
  getRiskByDimension: function () {
    return new Promise((resolve, reject) => {
      uaaAxios
        .get(`${apiURL}/risk/chartRiskByDimension`)
        .then((response) => {
          resolve(response.data);
          console.log(response);
          //resolve(getRiskByDimension);
        })
        .catch((err) => reject(err));
    });
  },
  getTopRisk: function () {
    return new Promise((resolve, reject) => {
      uaaAxios
        .get(`${apiURL}/risk/chartTopRisk`)
        .then((response) => {
          resolve(response.data);
          console.log(response);
          //resolve(getTopRisk);
        })
        .catch((err) => reject(err));
    });
  },
};
