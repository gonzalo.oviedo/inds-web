import Vue from "vue";
import uaaAxios from "@/utils/authAxios";

const fileDownload = require("js-file-download");
const DEFAULT_ALTERNATIVE_NAME = "download.pdf";

export default {
  getProjects: function () {
    return new Promise((resolve, reject) => {
      uaaAxios
        .get(`${process.env.VUE_APP_CALL_FOR_TENDER_API}/projects`)
        .then((response) => {
          resolve(Vue._.get(response, "data"));
        })
        .catch((err) => reject(err));
    });
  },

  getProject: function (projectId) {
    return new Promise((resolve, reject) => {
      uaaAxios
        .get(`${process.env.VUE_APP_CALL_FOR_TENDER_API}/projects/${projectId}`)
        .then((response) => {
          resolve(Vue._.get(response, "data"));
        })
        .catch((err) => reject(err));
    });
  },

  getSpecialistsProjects: function () {
    return new Promise((resolve, reject) => {
      uaaAxios
        .get(`${process.env.VUE_APP_CALL_FOR_TENDER_API}/projects/specialists`)
        .then((response) => {
          resolve(Vue._.get(response, "data"));
        })
        .catch((err) => reject(err));
    });
  },

  getStats: function (projectId) {
    return new Promise((resolve, reject) => {
      uaaAxios
        .get(
          `${process.env.VUE_APP_CALL_FOR_TENDER_API}/projects/${projectId}/stats`
        )
        .then((response) => {
          resolve(Vue._.get(response, "data"));
        })
        .catch((err) => reject(err));
    });
  },

  getSpecialistProject: function (projectId) {
    return new Promise((resolve, reject) => {
      uaaAxios
        .get(
          `${process.env.VUE_APP_CALL_FOR_TENDER_API}/projects/specialists/${projectId}`
        )
        .then((response) => {
          resolve(Vue._.get(response, "data"));
        })
        .catch((err) => reject(err));
    });
  },

  createProject: function (project) {
    return new Promise((resolve, reject) => {
      uaaAxios
        .post(`${process.env.VUE_APP_CALL_FOR_TENDER_API}/projects`, project)
        .then((response) => {
          resolve(Vue._.get(response, "data"));
        })
        .catch((err) => reject(err));
    });
  },

  adjudicar: function (projectId, postulationId) {
    return new Promise((resolve, reject) => {
      uaaAxios
        .post(
          `${process.env.VUE_APP_CALL_FOR_TENDER_API}/projects/toaward/${projectId}?tenderRequestId=${postulationId}`,
          {}
        )
        .then((response) => {
          resolve(Vue._.get(response, "data"));
        })
        .catch((err) => reject(err));
    });
  },

  publishProject: function (project) {
    return new Promise((resolve, reject) => {
      uaaAxios
        .post(
          `${process.env.VUE_APP_CALL_FOR_TENDER_API}/projects/publish`,
          project
        )
        .then((response) => {
          resolve(Vue._.get(response, "data"));
        })
        .catch((err) => reject(err));
    });
  },

  deleteProject: function (projectId) {
    return new Promise((resolve, reject) => {
      uaaAxios
        .delete(
          `${process.env.VUE_APP_CALL_FOR_TENDER_API}/projects/${projectId}`
        )
        .then((response) => {
          resolve(Vue._.get(response, "data"));
        })
        .catch((err) => reject(err));
    });
  },

  addAttachments(projectId, technicalRequirements, aditionalAttachment) {
    let formData = new FormData();
    if (technicalRequirements && technicalRequirements.changed) {
      formData.append("technicalRequirements", technicalRequirements);
    }
    if (aditionalAttachment && aditionalAttachment.changed) {
      formData.append("attachment", aditionalAttachment);
    }

    return new Promise((resolve, reject) => {
      uaaAxios
        .post(
          `${process.env.VUE_APP_CALL_FOR_TENDER_API}/projects/attachments/${projectId}`,
          formData
        )
        .then((response) => {
          resolve(Vue._.get(response, "data"));
        })
        .catch((err) => reject(err));
    });
  },

  saveRequestTender(requestTender) {
    console.log(
      "callForTenderService.js.saveRequestTender - called with requestTender:"
    );
    console.log(requestTender);

    requestTender = this.prepareRequestTender(requestTender);
    console.log(
      "callForTenderService.js.saveRequestTender - repared requestTender:"
    );
    console.log(requestTender);

    return new Promise((resolve, reject) => {
      uaaAxios
        .post(
          `${process.env.VUE_APP_CALL_FOR_TENDER_API}/tender-requests`,
          requestTender
        )
        .then((response) => {
          resolve(Vue._.get(response, "data"));
        })
        .catch((err) => reject(err));
    });
  },

  prepareRequestTender(requestTender) {
    console.log(
      "callForTenderService.js.prepareRequestTender - called with requestTender:"
    );
    console.log(requestTender);

    if (requestTender.budget == "" || requestTender.budget == null) {
      requestTender.budget = null;
    }
    if (requestTender.period == "" || requestTender.period == null) {
      requestTender.period = null;
    }
    if (
      requestTender.period.startDate == "" ||
      requestTender.period.startDate == null ||
      requestTender.period.endDate == "" ||
      requestTender.period.endDate == null
    ) {
      requestTender.period = null;
    }
    if (
      requestTender.teamCharacteristics == "" ||
      requestTender.teamCharacteristics == null
    ) {
      requestTender.teamCharacteristics = null;
    }
    if (
      requestTender.aditionalAttachments == "" ||
      requestTender.aditionalAttachments == null
    ) {
      requestTender.aditionalAttachments = null;
    }
    if (
      requestTender.processAndStrategyFile == "" ||
      requestTender.processAndStrategyFile == null
    ) {
      requestTender.processAndStrategyFile = null;
    }
    if (
      requestTender.economicalOffer == "" ||
      requestTender.economicalOffer == null
    ) {
      requestTender.economicalOffer = null;
    }
    if (
      requestTender.processAndStrategyFile == "" ||
      requestTender.processAndStrategyFile == null
    ) {
      requestTender.processAndStrategyFile = null;
    }
    if (requestTender.workplan == "" || requestTender.workplan == null) {
      requestTender.workplan = null;
    }
    if (
      requestTender.generalConsiderations == "" ||
      requestTender.generalConsiderations == null
    ) {
      requestTender.generalConsiderations = null;
    }

    return requestTender;
  },

  addTenderRequestAttachments(id, tenderRequest) {
    let formData = new FormData();
    let fileCount = 0;
    let {
      economicalOffer,
      processAndStrategyFile,
      generalConsiderations,
      teamCharacteristics,
      aditionalAttachments,
      workplan,
    } = tenderRequest;

    if (economicalOffer && !economicalOffer.id) {
      formData.append("economicalOffer", economicalOffer);
      fileCount++;
    }
    if (processAndStrategyFile && !processAndStrategyFile.id) {
      formData.append("processAndStrategyFile", processAndStrategyFile);
      fileCount++;
    }
    if (generalConsiderations && !generalConsiderations.id) {
      formData.append("generalConsiderations", generalConsiderations);
      fileCount++;
    }
    if (workplan && !workplan.id) {
      formData.append("workplan", workplan);
      fileCount++;
    }
    if (
      teamCharacteristics &&
      teamCharacteristics.length > 0 &&
      !teamCharacteristics[0].id
    ) {
      teamCharacteristics.map((f) => formData.append("teamCharacteristics", f));
      fileCount = fileCount + teamCharacteristics.length;
    }
    if (
      aditionalAttachments &&
      aditionalAttachments.length > 0 &&
      !aditionalAttachments[0].id
    ) {
      aditionalAttachments.map((f) =>
        formData.append("aditionalAttachments", f)
      );
      fileCount = fileCount + aditionalAttachments.length;
    }

    return new Promise((resolve, reject) => {
      if (fileCount) {
        uaaAxios
          .post(
            `${process.env.VUE_APP_CALL_FOR_TENDER_API}/tender-requests/attachments/${id}`,
            formData
          )
          //uaaAxios({baseURL: `/V3/attachments/${id}`,  method: 'post' , data: formData})
          .then((response) => {
            resolve(Vue._.get(response, "data"));
          })
          .catch((err) => reject(err));
      } else {
        resolve(tenderRequest);
      }
    });
  },

  getTenderRequest: function (id) {
    return new Promise((resolve, reject) => {
      uaaAxios
        .get(`${process.env.VUE_APP_CALL_FOR_TENDER_API}/tender-requests/${id}`)
        //uaaAxios({baseURL: `/V3/${id}`})
        .then((response) => {
          resolve(Vue._.get(response, "data"));
        })
        .catch((err) => reject(err));
    });
  },

  getLastRequestByProject: function (projectId) {
    return new Promise((resolve, reject) => {
      uaaAxios
        .get(
          `${process.env.VUE_APP_CALL_FOR_TENDER_API}/tender-requests?projectId=${projectId}`
        )
        //uaaAxios({baseURL: `/V3?projectId=${projectId}`})
        .then((response) => {
          resolve(Vue._.get(response, "data"));
        })
        .catch((err) => reject(err));
    });
  },

  completeTenderRequest: function (data) {
    return new Promise((resolve, reject) => {
      uaaAxios
        .post(
          `${process.env.VUE_APP_CALL_FOR_TENDER_API}/tender-requests/complete`,
          data
        )
        //uaaAxios( {baseURL : '/V3/complete', method : 'post', data})
        .then((response) => {
          resolve(Vue._.get(response, "data"));
        })
        .catch((err) => reject(err));
    });
  },

  downloadAttachment: function (attachmentId, alternativeName) {
    return new Promise((resolve, reject) => {
      uaaAxios
        .get(
          `${process.env.VUE_APP_CALL_FOR_TENDER_API}/attachments/${attachmentId}`,
          { responseType: "blob" }
        )
        //uaaAxios({baseURL: `/V2/${attachmentId}`,  method: 'get' , responseType: 'blob' })
        .then((response) => {
          let contentDisposition = response.headers["content-disposition"];
          alternativeName = alternativeName
            ? alternativeName
            : DEFAULT_ALTERNATIVE_NAME;

          let fileName = contentDisposition
            ? contentDisposition.split("=")[1]
            : alternativeName;
          fileDownload(response.data, fileName);
          resolve(Vue._.get(response, "data"));
        })
        .catch((err) => reject(err));
    });
  },

  getQuestions(requestId, page) {
    return new Promise((resolve, reject) => {
      uaaAxios
        .get(
          `${
            process.env.VUE_APP_CALL_FOR_TENDER_API
          }/tender-requests/${requestId}/questions?page=${page ? page : ""}`
        )
        //uaaAxios({baseURL: `/V3/${requestId}/questions?page=${page ? page : ''}`})
        .then((response) => {
          resolve(Vue._.get(response, "data"));
        })
        .catch((err) => reject(err));
    });
  },

  createQuestion(requestId, question) {
    return new Promise((resolve, reject) => {
      uaaAxios
        .post(
          `${process.env.VUE_APP_CALL_FOR_TENDER_API}/tender-requests/${requestId}/questions`,
          question
        )
        //uaaAxios({baseURL: `/V3/${requestId}/questions`, data: question, method: 'post'})
        .then((response) => {
          resolve(Vue._.get(response, "data"));
        })
        .catch((err) => reject(err));
    });
  },

  getMessages(requestId, chatId) {
    return new Promise((resolve, reject) => {
      uaaAxios
        .get(
          `${
            process.env.VUE_APP_CALL_FOR_TENDER_API
          }/tender-requests/${requestId}/chats/${chatId ? chatId : ""}`
        )
        //uaaAxios({baseURL: `/V3/${requestId}/chats/${chatId ? chatId : ''}`})
        .then((response) => {
          resolve(Vue._.get(response, "data"));
        })
        .catch((err) => reject(err));
    });
  },

  createMessage(requestId, message) {
    return new Promise((resolve, reject) => {
      uaaAxios
        .post(
          `${process.env.VUE_APP_CALL_FOR_TENDER_API}/tender-requests/${requestId}/chats`,
          message
        )
        //uaaAxios({baseURL: `/V3/${requestId}/chats`, method: 'post', data: message},)
        .then((response) => {
          resolve(Vue._.get(response, "data"));
        })
        .catch((err) => reject(err));
    });
  },

  cancelProject: function (projectId) {
    return new Promise((resolve, reject) => {
      uaaAxios
        .post(
          `${process.env.VUE_APP_CALL_FOR_TENDER_API}/projects/cancel?projectId=${projectId}`
        )
        .then((response) => {
          resolve(Vue._.get(response, "data"));
        })
        .catch((err) => reject(err));
    });
  },

  editPublishProject: function (project) {
    return new Promise((resolve, reject) => {
      uaaAxios
        .post(
          `${process.env.VUE_APP_CALL_FOR_TENDER_API}/projects/editProjectPublish`,
          project
        )
        //uaaAxios({baseURL: '/V1/publish', method: 'post', data: project})
        .then((response) => {
          resolve(Vue._.get(response, "data"));
        })
        .catch((err) => reject(err));
    });
  },
  deleteTender: function (id) {
    return new Promise((resolve, reject) => {
      uaaAxios
        .delete(
          `${process.env.VUE_APP_CALL_FOR_TENDER_API}/tender-requests?id=${id}`
        )
        .then((response) => {
          resolve(Vue._.get(response, "data"));
        })
        .catch((err) => reject(err));
    });
  },

  getMatchList: function (projecId) {
    return new Promise((resolve, reject) => {
      uaaAxios
        .get(
          `${process.env.VUE_APP_CALL_FOR_TENDER_API}/tender-match?projectId=` +
            projecId
        )
        .then((response) => {
          resolve(Vue._.get(response, "data"));
        })
        .catch((err) => reject(err));
    });
  },

  cancelRequest: function (id) {
    return new Promise((resolve, reject) => {
      uaaAxios
        .put(
          `${process.env.VUE_APP_CALL_FOR_TENDER_API}/tender-requests/cancel/${id}`
        )
        //uaaAxios({baseURL: '/V1/publish', method: 'post', data: project})
        .then((response) => {
          resolve(Vue._.get(response, "data"));
        })
        .catch((err) => reject(err));
    });
  },
  acceptEndPhase: function (id) {
    return new Promise((resolve, reject) => {
      uaaAxios
        .post(
          `${process.env.VUE_APP_CALL_FOR_TENDER_API}/projects/awardconfirm?tenderRequestId=${id}`
        )
        //uaaAxios({baseURL: '/V1/publish', method: 'post', data: project})
        .then((response) => {
          resolve(Vue._.get(response, "data"));
        })
        .catch((err) => reject(err));
    });
  },
};
