import Vue from "vue";
import uaaAxios from "@/utils/authAxios";
import companyService from "@/services/companyService";

export default {
  getByDimensionStrategy: function (dimension, strategy) {
    return new Promise((resolve, reject) => {
      companyService
        .getClientProfile()
        .then((clientProfile) => {
          let clientId = clientProfile.tributaryId;
          uaaAxios
            .get(
              `${process.env.VUE_APP_DT_API}/specialists/clients/${clientId}/dimensions/${dimension}/strategies/${strategy}`
            )
            .then((response) => resolve(Vue._.get(response, "data")))
            .catch((err) => reject(err));
        })
        .catch((err) => reject(err));
    });
  },
};
