import Vue from "vue";
import axios from "axios";
import qs from "qs";

export default {
  /**
   * Servicio para realizar login
   */
  login: function (username, password) {
    return new Promise((resolve) => {
      var data = qs.stringify({
        grant_type: "password",
        username: username,
        password: password,
        scope: "ui",
      });

      axios
        .post(process.env.VUE_APP_API_URL_BASE + "/uaa/oauth/token", data, {
          auth: {
            username: process.env.VUE_APP_CLIENT_ID,
            password: process.env.VUE_APP_SECRET,
          },
          headers: { "Content-Type": "application/x-www-form-urlencoded" },
        })
        .then((response) => {
          Vue.$log.debug("Http Status: ", Vue._.get(response, "status"));
          resolve(Vue._.get(response, "data"));
        })
        .catch((error) => {
          Vue.$log.error("Error Status: ", Vue._.get(error, "status"));
          resolve(error);
        });
    });
  },
  register: function (
    firstname,
    lastname,
    companyName,
    companyIdentifier,
    email,
    clientType,
    countryCode,
    source,
    token,
    businessParams,
    promo
  ) {
    return new Promise((resolve) => {
      let data = {
        username: email,
        firstName: firstname,
        lastName: lastname,
        businessParams,
        promo,
        client: {
          name: companyName,
          tributaryId: companyIdentifier,
          clientType: clientType,
          countryCode: countryCode,
          source: source,
        },
      };
      axios
        .post(
          `${
            process.env.VUE_APP_API_URL_BASE +
            process.env.VUE_APP_ONBOARDING_API
          }/users?recaptchaResponse=${token}`,
          data
        )
        .then((response) => {
          Vue.$log.debug("Http Status: ", Vue._.get(response, "status"));
          resolve(Vue._.get(response, "data"));
        })
        .catch((error) => {
          Vue.$log.error("Error Status: ", Vue._.get(error, "status"));
          resolve(error);
        });
    });
  },

  activate: function (userId, password) {
    return new Promise((resolve) => {
      axios
        .post(
          `${
            process.env.VUE_APP_API_URL_BASE +
            process.env.VUE_APP_ONBOARDING_API
          }/activate/${userId}`,
          null,
          {
            params: { password: password },
          }
        )
        .then((response) => {
          Vue.$log.debug("Http Status: ", Vue._.get(response, "status"));
          resolve(Vue._.get(response, "data"));
        })
        .catch((error) => {
          Vue.$log.error("Error Status: ", Vue._.get(error, "status"));
          resolve(error);
        });
    });
  },

  validateUser: function (userId) {
    return new Promise((resolve) => {
      axios
        .get(
          `${
            process.env.VUE_APP_API_URL_BASE +
            process.env.VUE_APP_ONBOARDING_API
          }/activate/${userId}`
        )
        .then((response) => {
          Vue.$log.debug("Http Status: ", Vue._.get(response, "status"));
          resolve(Vue._.get(response, "data"));
        })
        .catch((error) => {
          Vue.$log.error("Error Status: ", Vue._.get(error, "status"));
          resolve(error);
        });
    });
  },

  /**
   * Valida el token obtenido
   */
  validateToken: function (token) {
    if (token && Vue._.get(token, "length") > 0) {
      return new Promise((resolve, reject) => {
        axios
          .get(process.env.VUE_APP_API_URL_BASE + "/uaa/oauth/check_token", {
            params: { token },
            auth: {
              username: process.env.VUE_APP_CLIENT_ID,
              password: process.env.VUE_APP_SECRET,
            },
          })
          .then(() => resolve(true))
          .catch((err) => reject(err));
      });
    }
  },

  sendPasswordRecoveryEmail: function (email, captcha) {
    return new Promise((resolve, reject) => {
      axios
        .post(
          `${
            process.env.VUE_APP_API_URL_BASE + process.env.VUE_APP_UMAPI_API
          }/passwords/recovery`,
          {
            username: email,
            recaptcha: captcha,
          }
        )
        .then((response) => resolve(Vue._.get(response, "data")))
        .catch((err) => reject(err));
    });
  },

  checkPasswordToken(token) {
    return new Promise((resolve, reject) => {
      axios
        .post(
          `${
            process.env.VUE_APP_API_URL_BASE + process.env.VUE_APP_UMAPI_API
          }/users/check-password-token/${token}`
        )
        .then((response) => resolve(Vue._.get(response, "data")))
        .catch((err) => reject(err));
    });
  },

  initChangePassword(id) {
    return new Promise((resolve, reject) => {
      axios
        .get(
          `${
            process.env.VUE_APP_API_URL_BASE + process.env.VUE_APP_UMAPI_API
          }/passwords/recovery/${id}`
        )
        .then((response) => resolve(Vue._.get(response, "data")))
        .catch((err) => reject(err));
    });
  },

  recoverPasswordByToken(id, pass, confirm, captcha) {
    return new Promise((resolve, reject) => {
      axios
        .post(
          `${
            process.env.VUE_APP_API_URL_BASE + process.env.VUE_APP_UMAPI_API
          }/passwords/recover-password/`,
          {
            recoveryPasswordId: id,
            password: pass,
            passwordConfirm: confirm,
            recaptcha: captcha,
          }
        )
        .then((response) => resolve(Vue._.get(response, "data")))
        .catch((err) => reject(err));
    });
  },
};
