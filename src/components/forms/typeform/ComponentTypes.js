import DtiFormTemplate from "./DtiFormTemplate.vue";
import InputBox from "./form-elements/fields/InputBox.vue";
import TextArea from "./form-elements/fields/TextArea.vue";
import RadioButton from "./form-elements/fields/RadioButton.vue";
import CheckBox from "./form-elements/fields/CheckBox.vue";
import Select from "./form-elements/fields/Select.vue";

const COMPONENT_MAP = {
  dtiFormTemplate: DtiFormTemplate,
  text: InputBox,
  textarea: TextArea,
  radio: RadioButton,
  check: CheckBox,
  select: Select,
};

export function getComponent(type) {
  return COMPONENT_MAP[type];
}
