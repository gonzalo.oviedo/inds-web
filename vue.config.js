const webpack = require("webpack");

module.exports = {
  // config function var
  configureWebpack: () => {
    return {
      plugins: [
        new webpack.DefinePlugin({
          "process.env": {
            VUE_APP_TWAY_VERSION: JSON.stringify(
              require("./package.json").version
            ),
          },
        }),
      ],
    };
  },
  devServer: {
    proxy: {
      "/V1": {
        target: "http://localhost:8001/cft-api/projects",
        changeOrigin: true,
        pathRewrite: {
          "^/V1": "",
        },
      },
      "/V2": {
        target: "http://localhost:8001/cft-api/attachments",
        changeOrigin: true,
        pathRewrite: {
          "^/V2": "",
        },
      },
      "/V3": {
        target: "http://localhost:8001/cft-api/tender-requests",
        changeOrigin: true,
        pathRewrite: {
          "^/V3": "",
        },
      },
    },
  },
  transpileDependencies: ["vuetify"],
};
